USE [CanvasDB]
GO
ALTER TABLE [dbo].[tblUser] DROP CONSTRAINT [FK_tblUser_tblRole]
GO
ALTER TABLE [dbo].[tblSubject] DROP CONSTRAINT [FK_tblSubject_tblCourse]
GO
ALTER TABLE [dbo].[tblStudentCourse] DROP CONSTRAINT [FK_tblStudentCourse_tblClassCourse]
GO
ALTER TABLE [dbo].[tblStudentAnswer] DROP CONSTRAINT [FK_tblStudentAnswer_tblStudentCourse]
GO
ALTER TABLE [dbo].[tblStudentAnswer] DROP CONSTRAINT [FK_tblStudentAnswer_tblQuiz]
GO
ALTER TABLE [dbo].[tblStudentAnswer] DROP CONSTRAINT [FK_tblStudentAnswer_tblAnswer]
GO
ALTER TABLE [dbo].[tblQuiz] DROP CONSTRAINT [FK_tblQuiz_tblSubject1]
GO
ALTER TABLE [dbo].[tblQuestion] DROP CONSTRAINT [FK_tblQuestion_tblSubject]
GO
ALTER TABLE [dbo].[tblLession] DROP CONSTRAINT [FK_tblLession_tblSubject]
GO
ALTER TABLE [dbo].[tblFile] DROP CONSTRAINT [FK_tblFile_tblLession]
GO
ALTER TABLE [dbo].[tblFeedback] DROP CONSTRAINT [FK_tblFeedback_tblClassCourse]
GO
ALTER TABLE [dbo].[tblEvent] DROP CONSTRAINT [FK_tblEvent_tblClassCourse]
GO
ALTER TABLE [dbo].[tblCourse] DROP CONSTRAINT [FK_tblCourse_tblUser]
GO
ALTER TABLE [dbo].[tblClassCourse] DROP CONSTRAINT [FK_tblClassCourse_tblCourse]
GO
ALTER TABLE [dbo].[tblClassCourse] DROP CONSTRAINT [FK_tblClassCourse_tblClass]
GO
ALTER TABLE [dbo].[tblAnswer] DROP CONSTRAINT [FK_tblAnswer_tblQuestion]
GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblUser]
GO
/****** Object:  Table [dbo].[tblSubject]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblSubject]
GO
/****** Object:  Table [dbo].[tblStudentCourse]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblStudentCourse]
GO
/****** Object:  Table [dbo].[tblStudentAnswer]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblStudentAnswer]
GO
/****** Object:  Table [dbo].[tblRole]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblRole]
GO
/****** Object:  Table [dbo].[tblQuiz]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblQuiz]
GO
/****** Object:  Table [dbo].[tblQuestion]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblQuestion]
GO
/****** Object:  Table [dbo].[tblLession]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblLession]
GO
/****** Object:  Table [dbo].[tblFile]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblFile]
GO
/****** Object:  Table [dbo].[tblFeedback]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblFeedback]
GO
/****** Object:  Table [dbo].[tblEvent]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblEvent]
GO
/****** Object:  Table [dbo].[tblCourse]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblCourse]
GO
/****** Object:  Table [dbo].[tblClassCourse]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblClassCourse]
GO
/****** Object:  Table [dbo].[tblClass]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblClass]
GO
/****** Object:  Table [dbo].[tblAnswer]    Script Date: 8/24/2021 8:40:35 PM ******/
DROP TABLE [dbo].[tblAnswer]
GO
/****** Object:  Table [dbo].[tblAnswer]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblAnswer](
	[answerID] [int] IDENTITY(1,1) NOT NULL,
	[questionID] [int] NOT NULL,
	[answerContent] [nvarchar](500) NOT NULL,
	[isRight] [bit] NOT NULL,
 CONSTRAINT [PK_tblAnswer] PRIMARY KEY CLUSTERED 
(
	[answerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblClass]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClass](
	[classID] [int] IDENTITY(1,1) NOT NULL,
	[createDate] [date] NOT NULL,
	[className] [nvarchar](50) NOT NULL,
	[isActive] [bit] NOT NULL,
	[numberOfStudent] [int] NOT NULL,
 CONSTRAINT [PK_tblClassza] PRIMARY KEY CLUSTERED 
(
	[classID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblClassCourse]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblClassCourse](
	[classCourseID] [int] IDENTITY(1,1) NOT NULL,
	[classID] [int] NULL,
	[courseID] [int] NULL,
 CONSTRAINT [PK_tblClassCourse] PRIMARY KEY CLUSTERED 
(
	[classCourseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblCourse]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblCourse](
	[courseID] [int] IDENTITY(1,1) NOT NULL,
	[courseName] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[createUserID] [nvarchar](50) NOT NULL,
	[startAt] [date] NOT NULL,
	[isActive] [bit] NOT NULL,
	[createDate] [date] NOT NULL,
 CONSTRAINT [PK_tblCourseS] PRIMARY KEY CLUSTERED 
(
	[courseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblEvent]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEvent](
	[eventID] [int] IDENTITY(1,1) NOT NULL,
	[eventName] [nvarchar](50) NOT NULL,
	[description] [nvarchar](50) NOT NULL,
	[classCourseID] [int] NOT NULL,
	[createDate] [date] NOT NULL,
	[eventURL] [nvarchar](250) NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_tblEvent] PRIMARY KEY CLUSTERED 
(
	[eventID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblFeedback]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFeedback](
	[fbID] [int] IDENTITY(1,1) NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[classCourseID] [int] NOT NULL,
	[message] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_tblFeedback] PRIMARY KEY CLUSTERED 
(
	[fbID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblFile]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblFile](
	[fileName] [nvarchar](100) NOT NULL,
	[lessionID] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblLession]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblLession](
	[lessionID] [int] IDENTITY(1,1) NOT NULL,
	[subjectID] [int] NOT NULL,
 CONSTRAINT [PK_tblLession] PRIMARY KEY CLUSTERED 
(
	[lessionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblQuestion]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQuestion](
	[questionID] [int] IDENTITY(1,1) NOT NULL,
	[subjectID] [int] NOT NULL,
	[createDate] [date] NOT NULL,
	[createUserID] [nvarchar](50) NOT NULL,
	[questionContent] [nvarchar](500) NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_tblQuestionS] PRIMARY KEY CLUSTERED 
(
	[questionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblQuiz]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblQuiz](
	[quizID] [int] IDENTITY(1,1) NOT NULL,
	[subjectID] [int] NOT NULL,
	[quizName] [nvarchar](50) NOT NULL,
	[quizNum] [int] NOT NULL,
	[quizTime] [nvarchar](50) NOT NULL,
	[startAt] [datetime] NOT NULL,
	[endAt] [datetime] NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_tblQuiz] PRIMARY KEY CLUSTERED 
(
	[quizID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblRole]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblRole](
	[roleID] [int] IDENTITY(1,1) NOT NULL,
	[roleName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_tblRole] PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblStudentAnswer]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStudentAnswer](
	[studentJoinID] [nvarchar](50) NOT NULL,
	[answerID] [int] NOT NULL,
	[quizID] [int] NOT NULL,
	[correctNum] [int] NOT NULL,
	[mark] [float] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblStudentCourse]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblStudentCourse](
	[studentJoinID] [nvarchar](50) NOT NULL,
	[classCourseID] [int] NULL,
	[courseMark] [float] NOT NULL,
	[joinDate] [date] NOT NULL,
 CONSTRAINT [PK_tblStudentCourseS] PRIMARY KEY CLUSTERED 
(
	[studentJoinID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblSubject]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblSubject](
	[subjectID] [int] IDENTITY(1,1) NOT NULL,
	[subjectName] [nvarchar](50) NOT NULL,
	[courseID] [int] NOT NULL,
	[isActive] [bit] NULL,
 CONSTRAINT [PK_tblSubject] PRIMARY KEY CLUSTERED 
(
	[subjectID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblUser]    Script Date: 8/24/2021 8:40:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblUser](
	[userID] [nvarchar](50) NOT NULL,
	[roleID] [int] NOT NULL,
	[fullName] [nvarchar](50) NOT NULL,
	[email] [nvarchar](50) NOT NULL,
	[phoneNum] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[createDate] [date] NOT NULL,
 CONSTRAINT [PK_tblUsers] PRIMARY KEY CLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tblAnswer] ON 

INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (1, 1, N'A. An object', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (2, 1, N'B. A data field', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (3, 1, N'C. A class', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (4, 1, N'D. A method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (5, 3, N'A. A class', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (6, 3, N'B. A method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (7, 3, N'C. An object', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (8, 3, N'D. A data field', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (9, 4, N'A. data', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (10, 4, N'B. class', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (11, 4, N'C. program', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (12, 4, N'D. method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (13, 7, N'A. private', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (14, 7, N'B. class', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (15, 7, N'C. public', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (16, 7, N'D. All of the above.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (17, 9, N'A. A constructor', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (18, 9, N'B. A method with the void return type', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (19, 9, N'C. A method with a return type', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (20, 9, N'D. The main method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (21, 10, N'A. x contains an int value.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (22, 10, N'B. You can assign an int value to x.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (23, 10, N'C. x contains an object of the Circle type.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (24, 10, N'D. x contains a reference to a Circle object.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (25, 12, N'A. true, 1, null', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (26, 12, N'B. false, 1, null', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (27, 12, N'C. false, 0, null', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (28, 12, N'D. true, 1, Null', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (29, 14, N'A. always accepts two arguments', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (30, 14, N'B. has return type of void', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (31, 14, N'C. has the same name as the class', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (32, 14, N'D. always has an access specifier of private', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (33, 15, N'A. An object of the Automobile class is created.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (34, 15, N'B. An object of the Automobile class is declared, but not created.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (35, 15, N'C. A constructor that accepts a string in the Automobile class is called.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (36, 15, N'D. The default constructor in the Automobile class is called.
', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (37, 16, N'A. None', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (38, 16, N'B. Employee() {...}', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (39, 16, N'C. Employee(String name) {...}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (41, 16, N'D. Both B and C', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (42, 17, N'A. None', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (43, 17, N'B. Employee() {...}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (44, 17, N'C. Employee(String name) {...}
', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (45, 17, N'D. Both B and C', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (50, 24, N'A. By getting a Set object from the Map and iterating through it.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (51, 24, N'B. By iterating through the Iterator of the Map.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (52, 24, N'C. By enumerating through the Enumeration of the Map.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (53, 24, N'D. By getting a List from the Map and enumerating through the List.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (54, 25, N'A. private', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (55, 25, N'B. volatile', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (56, 25, N'C. protected', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (57, 25, N'D. transient', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (58, 26, N'A. True', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (59, 26, N'B. False', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (60, 27, N'A. public, private, protected, default', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (61, 27, N'B. default, protected, private, public', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (62, 27, N'C. public, default, protected, private', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (63, 27, N'D. public, protected, default, private', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (64, 28, N'A. public', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (65, 28, N'B. static', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (66, 28, N'C. native', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (67, 28, N'D. transient', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (68, 29, N'A. A final object''s data cannot be changed.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (69, 29, N'B. A final class can be subclassed.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (70, 29, N'C. A final method cannot be overloaded.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (71, 29, N'D. A final object cannot be reassigned a new address in memory.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (72, 30, N'A. "is a"', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (73, 30, N'B. "has a"', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (74, 30, N'C. "was a"', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (75, 30, N'D. "will be a"', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (76, 31, N'A. this', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (77, 31, N'B. super', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (78, 31, N'C. final', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (79, 31, N'D. static', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (80, 32, N'A. 23', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (82, 32, N'B. 4', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (83, 32, N'C. 5.3', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (84, 32, N'D. 3', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (85, 33, N'A. int x = -1; x = x >>> 5;', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (86, 33, N'B. int x = -1; x = x >>> 32;', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (87, 33, N'C. byte x = -1; x = x >>> 5;', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (88, 33, N'D. int x = -1; x = x >> 5;', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (89, 35, N'A. >>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (90, 35, N'B. >>>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (91, 35, N'C. <<', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (92, 35, N'D. None of these', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (93, 36, N'A. +', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (94, 36, N'B. -', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (95, 36, N'C. *', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (96, 36, N'D. /', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (97, 37, N'A. A reference', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (98, 37, N'B. A class', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (99, 37, N'C. An interface', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (100, 37, N'D. A variable of primitive type', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (101, 38, N'A. A reference', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (102, 38, N'B. A class', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (103, 38, N'C. An interface', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (104, 38, N'D. Both B and C', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (105, 39, N'A. A negative number with very large magnitude.', 0)
GO
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (106, 39, N'B. A positive number with very large magnitude.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (107, 39, N'C. -25', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (108, 39, N'D. -100', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (109, 40, N'A. Transient variables are not serialized.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (110, 40, N'B. Transient methods must be overridden.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (111, 40, N'C. Transient classes may not be serialized.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (112, 40, N'D. Transient variables must be static.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (113, 41, N'A. private', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (114, 41, N'B. protected', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (115, 41, N'C. private protected', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (116, 41, N'D. transient', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (117, 42, N'A. An abstract class may be instantiated.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (118, 42, N'B. An abstract class must contain at least one abstract method.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (119, 42, N'C. An abstract class must contain at least one abstract data field.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (120, 42, N'D. None of the above.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (121, 43, N'A. A final class must be instantiated.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (122, 43, N'B. A final class may only contain final methods.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (123, 43, N'C. A final class may not contain non-final data fields.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (124, 43, N'D. A final class may not be extended.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (125, 44, N'A. Only primitives are converted automatically; to change the type of an object reference, you have to do a cast.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (126, 44, N'B. Only object references are converted automatically; to change the type of a primitive, you have to do a cast.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (127, 44, N'C. Arithmetic promotion of object references requires explicit casting.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (128, 44, N'D. Both primitives and object references can be both converted and cast.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (129, 45, N'A. Object references can be converted in assignments but not in method calls.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (130, 45, N'B. Object references can be converted in method calls but not in assignments.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (131, 45, N'C. Object references can be converted in both method calls and assignments, but the rules governing these conversions are very different.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (132, 45, N'D. Object references can be converted in both method calls and assignments, and the rules governing these conversions are identical.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (133, 46, N'A. Classes and Interfaces', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (134, 46, N'B. Arrays of classes', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (135, 46, N'C. Arrays of interfaces', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (136, 46, N'D. All of the above', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (137, 47, N'A. Abstract classes', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (138, 47, N'B. Final classes', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (139, 47, N'C. Primitives', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (140, 47, N'D. All of the above', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (141, 48, N'A. Sometimes', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (142, 48, N'B. Always', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (143, 48, N'C. Never', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (144, 48, N'D. Usually', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (145, 49, N'A. Always', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (146, 49, N'B. Sometimes', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (147, 49, N'C. When neither x nor y is a float, a long, or a double

', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (148, 49, N'D. Never', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (149, 50, N'A. short', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (150, 50, N'B. int', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (151, 50, N'. There are no possible legal types.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (152, 50, N'D. long', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (153, 51, N'A. Positive', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (154, 51, N'B. Zero', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (155, 51, N'C. Negative', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (156, 51, N'D. All of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (157, 52, N'A. There is no difference; the rules are the same.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (158, 52, N'B. Method-call conversion supports narrowing, assignment conversion does not.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (159, 52, N'C. Assignment conversion supports narrowing, method-call conversion does not.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (160, 52, N'D. Method-call conversion supports narrowing if the method declares that it throws
ClassCastException.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (161, 53, N'A. Always', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (162, 53, N'B. When the exception is being thrown in response to catching of a different exception type', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (163, 53, N'C. When the exception is being thrown from a public method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (164, 53, N'D. When the exception is being thrown from a private method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (165, 54, N'A. Runtime exceptions', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (166, 54, N'B. Checked exceptions', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (167, 54, N'C. Assertion errors', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (168, 54, N'D. Errors other than assertion errors', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (169, 55, N'A. When the exception is constructed', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (170, 55, N'B. When the exception is thrown', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (171, 55, N'C. When the exception is caught', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (172, 55, N'D. When the exception''s printStackTrace() method is called', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (173, 56, N'A. When a public method''s preconditions are violated', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (174, 56, N'B. When a public method''s postconditions are violated', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (175, 56, N'C. When a nonpublic method''s preconditions are violated', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (176, 56, N'D. Never', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (177, 57, N'A. void xyz(float f)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (178, 57, N'B. public void xyz(float f)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (179, 57, N'C. private void xyz(float f)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (180, 57, N'D. Both A and B', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (181, 58, N'A. if (x == y)', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (182, 58, N'B. if (x.equals(y))', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (183, 58, N'C. if (x.toString().equals(y.toString()))', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (184, 58, N'D. if (x.hashCode() == y.hashCode())', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (185, 59, N'A. They must be defined inside a code block.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (186, 59, N'B. They may only read and write final variables of the enclosing class.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (187, 59, N'C. They may only call final methods of the enclosing class.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (188, 59, N'D. They may not call the enclosing class'' synchronized methods.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (189, 60, N'A. getName()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (190, 60, N'B. name()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (191, 60, N'C. toString()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (192, 60, N'D. Both B and C', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (193, 61, N'A. public void doSomething(int a, float b) { ... }', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (194, 61, N'B. private void doSomething(int a, float b) { ... }', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (195, 61, N'C. public void doSomething(int a, float b) throws java.io.IOException { ... }', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (196, 61, N'D. private void doSomething(int a, float b) throws java.io.IOException { ... }', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (197, 62, N'A. suspend() and resume()', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (198, 62, N'B. wait() and notify()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (199, 62, N'C. start() and stop()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (200, 62, N'D. sleep() and yield()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (201, 63, N'A. Every thread starts executing with a priority of 5.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (202, 63, N'B. Threads inherit their priority from their parent thread.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (203, 63, N'C. Threads are guaranteed to run with the priority that you set using the setPriority()
method.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (204, 63, N'D. Thread priority is an integer ranging from 1 to 100.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (205, 64, N'A. The wait() and notify() methods can be called outside synchronized code.', 0)
GO
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (206, 64, N'B. The programmer can specify which thread should be notified in a notify() method call.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (207, 64, N'C. The thread that calls wait() goes into the monitor''s pool of waiting threads.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (208, 64, N'D. The thread that calls notify() gives up the lock.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (209, 65, N'A. One', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (210, 65, N'B. One for each method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (211, 65, N'C. One for each synchronized method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (212, 65, N'D. One for each non-static synchronized method', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (213, 66, N'A. Mark all variables as synchronized.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (214, 66, N'B. Mark all variables as volatile.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (215, 66, N'C. Use only static variables.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (216, 66, N'D. Access the variables only via synchronized methods.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (217, 67, N'A. Synchronize access to all shared variables.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (218, 67, N'B. Make sure all threads yield from time to time.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (219, 67, N'C. Vary the priorities of your threads.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (220, 67, N'D. A, B, and C do not ensure that multithreaded code does not deadlock.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (221, 68, N'A. The strategy works.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (222, 68, N'B. The strategy works, provided the new methods are public.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (223, 68, N'C. The strategy works, provided the new methods are not private.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (224, 68, N'D. The strategy fails because you cannot subclass java.lang.Math.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (225, 69, N'A. java.util.ArrayList', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (226, 69, N'B. java.util.HashList', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (227, 69, N'C. java.util.StackL', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (228, 69, N'D. Both A and C', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (232, 70, N'A. public', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (233, 70, N'B. protected', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (234, 70, N'C. default', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (235, 70, N'D. private', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (236, 71, N'A. public', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (238, 71, N'B. protected', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (239, 71, N'C. default', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (240, 71, N'D. private', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (241, 72, N'A. A must have a no-args constructor.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (242, 72, N'B. B must have a no-args constructor.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (243, 72, N'C. C must have a no-args constructor.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (244, 72, N'D. There are no restrictions regarding no-args constructors.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (245, 73, N'A. A must have a no-args constructor.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (246, 73, N'B. B must have a no-args constructor.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (247, 73, N'C. C must have a no-args constructor.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (248, 73, N'D. There are no restrictions regarding no-args constructors.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (249, 74, N'A. JavaScript is a stripped-down version of Java', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (250, 74, N'B. JavaScript''s syntax is loosely based on Java''s', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (251, 74, N'C. They both originated on the island of Java', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (252, 74, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (253, 75, N'A. The User''s machine running a Web browser', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (254, 75, N'B. The Web server', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (255, 75, N'C. A central machine deep within Netscape''s corporate offices', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (256, 75, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (257, 77, N'A. Microsoft', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (258, 77, N'B. Navigator', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (259, 77, N'C. LiveWire', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (260, 77, N'D. Native', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (261, 78, N'A. Microsoft', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (262, 78, N'B. Navigator', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (263, 78, N'C. LiveWire', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (264, 78, N'D. Native', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (265, 79, N'A. Storing numbers, dates, or other values', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (266, 79, N'B. Varying randomly', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (267, 79, N'C. Causing high-school algebra flashbacks', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (268, 79, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (269, 80, N'A. Client-side', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (270, 80, N'B. Server-side', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (271, 80, N'C. Local', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (272, 80, N'D. Native', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (273, 81, N'A. The </script>', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (274, 81, N'B. The <script>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (275, 81, N'C. The END statement', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (276, 81, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (277, 82, N'A. Validating a form', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (278, 82, N'B. Sending a form''s contents by email', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (279, 82, N'C. Storing the form''s contents to a database file on the server', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (280, 82, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (281, 83, N'A. Return a value', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (282, 83, N'B. Accept parameters and Return a value', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (283, 83, N'C. Accept parameters', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (284, 83, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (285, 84, N'A. 2names', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (286, 84, N'B. _first_and_last_names', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (287, 84, N'C. FirstAndLast', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (288, 84, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (289, 85, N'A. <SCRIPT>', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (290, 85, N'B. <BODY>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (291, 85, N'C. <HEAD>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (292, 85, N'D. <TITLE>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (293, 86, N'A. The number of milliseconds since January 1st, 1970', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (294, 86, N'B. The number of days since January 1st, 1900', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (295, 86, N'C. The number of seconds since Netscape''s public stock offering.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (296, 86, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (297, 87, N'A. LANGUAGE', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (298, 87, N'B. SCRIPT', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (299, 87, N'C. VERSION', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (300, 87, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (301, 88, N'A. System.out.println("Hello World")', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (302, 88, N'B. println ("Hello World")', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (303, 88, N'C. document.write("Hello World")', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (304, 88, N'D. response.write("Hello World")', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (305, 89, N'A. <LANGUAGE="JavaScriptVersion">', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (306, 89, N'B. <SCRIPT LANGUAGE="JavaScriptVersion">', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (307, 89, N'C. <SCRIPT LANGUAGE="JavaScriptVersion"> JavaScript
statements...</SCRIPT>', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (308, 89, N'D. <SCRIPT LANGUAGE="JavaScriptVersion"!> JavaScript statements...
</SCRIPT>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (309, 90, N'A. <js>', 0)
GO
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (310, 90, N'B. <scripting>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (311, 90, N'C. <script>', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (312, 90, N'D. <javascript>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (313, 91, N'A. <script href=" abc.js">', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (314, 91, N'B. <script name=" abc.js">', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (315, 91, N'C. <script src=" abc.js">', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (316, 91, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (317, 92, N'A. Server-side image maps', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (318, 92, N'B. Client-side image maps', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (319, 92, N'C. Server-side image maps and Client-side image maps', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (320, 92, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (321, 93, N'A. navigator.appCodeName', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (322, 93, N'B. navigator.appName', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (323, 93, N'C. navigator.appVersion', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (324, 93, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (325, 94, N'var txt = new Array("tim","kim","jim")', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (326, 94, N'B. var txt = new Array:1=("tim")2=("kim")3=("jim")', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (327, 94, N'C. var txt = new Array("tim","kim","jim")', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (328, 94, N'D. var txt = new Array="tim","kim","jim"', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (329, 95, N'A. Enclose text to be displayed by non-JavaScript browsers.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (330, 95, N'B. Prevents scripts on the page from executing.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (331, 95, N'C. Describes certain low-budget movies.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (332, 95, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (333, 96, N'A. "New Text"?', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (334, 96, N'B. para1.value="New Text";', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (335, 96, N'C. para1.firstChild.nodeValue= "New Text";', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (336, 96, N'D. para1.nodeValue="New Text";', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (337, 97, N'A. Semicolon, colon', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (338, 97, N'B. Semicolon, Ampersand', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (339, 97, N'C. Ampersand, colon', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (340, 97, N'D. Ampersand, semicolon', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (341, 98, N'A. a low-level programming language.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (342, 98, N'B. a scripting language precompiled in the browser.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (343, 98, N'C. a compiled scripting language.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (344, 98, N'D. an object-oriented scripting language.', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (345, 99, N'A. FileUpLoad', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (346, 99, N'B. Function', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (347, 99, N'C. File', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (348, 99, N'D. Date', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (349, 100, N'A. Database', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (350, 100, N'B. Cursor', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (351, 100, N'C. Client', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (352, 100, N'D. FileUpLoad', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (353, 101, N'A. new', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (354, 101, N'B. this', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (355, 101, N'C. delete', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (356, 101, N'D. typeof', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (357, 102, N'A. Eval', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (358, 102, N'B. ParseInt', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (359, 102, N'C. ParseFloat', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (360, 102, N'D. Efloat', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (361, 103, N'A. onfocus', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (362, 103, N'B. onblur', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (363, 103, N'C. onclick', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (364, 103, N'D. ondblclick', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (365, 104, N'A. [objectName.]eval(numeric)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (366, 104, N'B. [objectName.]eval(string)', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (367, 104, N'C. [EvalName.]eval(string)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (368, 104, N'D. [EvalName.]eval(numeric)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (369, 105, N'A. Client', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (370, 105, N'B. Server', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (371, 105, N'C. Object', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (372, 105, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (373, 106, N'A. Select', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (374, 106, N'B. If', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (375, 106, N'C. Switch', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (376, 106, N'D. For', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (377, 107, N'A. if (conditional expression is true) thenexecute this codeend if', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (378, 107, N'B. if (conditional expression is true)execute this codeend if', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (379, 107, N'C. if (conditional expression is true) {then execute this code>->}', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (380, 107, N'D. if (conditional expression is true) then {execute this code}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (381, 108, N'A. dateObjectName = new Date([parameters])', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (382, 108, N'B. dateObjectName.new Date([parameters])', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (383, 108, N'C. dateObjectName := new Date([parameters])', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (384, 108, N'D. dateObjectName Date([parameters])', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (385, 109, N'A. Reverse', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (386, 109, N'B. Shift', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (387, 109, N'C. Slice', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (388, 109, N'D. Splice', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (389, 110, N'A. window.captureEvents(Event.CLICK);', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (390, 110, N'B. window.handleEvents (Event.CLICK);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (391, 110, N'C. window.routeEvents(Event.CLICK );', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (392, 110, N'D. window.raiseEvents(Event.CLICK );', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (393, 111, N'A. <IMG>', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (394, 111, N'B. <A>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (395, 111, N'C. <BR>', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (396, 111, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (397, 112, N'A. Pathname', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (398, 112, N'B. Protocol', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (399, 112, N'C. Defaultstatus', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (400, 112, N'D. Host', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (401, 113, N'A. ENABLE_TAINT', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (402, 113, N'B. MS_ENABLE_TAINT', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (403, 113, N'C. NS_ENABLE_TAINT', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (404, 113, N'D. ENABLE_TAINT_NS', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (405, 114, N'A. a wrapper', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (406, 114, N'B. a link', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (407, 114, N'C. a cursor', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (408, 114, N'D. a form', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (409, 115, N'A. ScriptObject', 0)
GO
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (410, 115, N'B. JSObject', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (411, 115, N'C. JavaObject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (412, 115, N'D. Jobject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (413, 116, N'A. ScriptObject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (414, 116, N'B. JSObject', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (415, 116, N'C. JavaObject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (416, 116, N'D. Jobject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (417, 117, N'A. JavaArray', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (418, 117, N'B. JavaClass', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (419, 117, N'C. JavaObject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (420, 117, N'D. JavaPackage', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (421, 118, N'A. JavaArray', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (422, 118, N'B. JavaClass', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (423, 118, N'C. JavaObject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (424, 118, N'D. JavaPackage', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (425, 119, N'A. netscape.javascript.JSObject', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (426, 119, N'B. netscape.javascript.JSException', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (427, 119, N'C. netscape.plugin.JSException', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (428, 119, N'D. None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (429, 120, N'A. user_pref(" javascript.console.open_on_error", false);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (430, 120, N'B. user_pref("javascript.console.open_error ", true);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (431, 120, N'C. user_pref("javascript.console.open_error ", false);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (432, 120, N'D. user_pref("javascript.console.open_on_error", true);', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (433, 121, N'A. user_pref("javascript.classic.error_alerts", true);', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (434, 121, N'B. user_pref("javascript.classic.error_alerts ", false);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (435, 121, N'C. user_pref("javascript.console.open_on_error ", true);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (436, 121, N'D. user_pref("javascript.console.open_on_error ", false);', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (437, 122, N'A. Blur()', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (438, 122, N'B. Blur(contrast)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (439, 122, N'C. Blur(value)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (440, 122, N'D. Blur(depth)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (441, 123, N'A. captureEvents()', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (442, 123, N'B. captureEvents(args eventType)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (443, 123, N'C. captureEvents(eventType)', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (444, 123, N'D. captureEvents(eventVal)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (445, 124, N'A. Close(doc)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (446, 124, N'B. Close(object)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (447, 124, N'C. Close(val)', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (448, 124, N'D. Close()', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (449, 125, N'A) Top and bottom margin will be 10px and left and right margin will be 2% of the total width.', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (450, 125, N'B) Left and right margin will be 10px and top and bottom margin will be 2% of the total height', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (451, 125, N'C) Top margin will be 10px and other margins will be 2% of the total width', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (452, 125, N'D) Left margin will be 10px and other margins will be 2% of the total width', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (453, 126, N'A) list-style-type', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (454, 126, N'B) list-style-layout', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (455, 126, N'C) list-type-style', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (456, 126, N'D) list-type', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (457, 127, N'A) list-spacing', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (458, 127, N'B) marker-spacing', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (459, 127, N'C) marker-offset', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (460, 127, N'D) list-offset', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (461, 128, N'A) Spacing', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (462, 128, N'B) Marking', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (463, 128, N'C) Padding', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (464, 128, N'D) Content-border', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (465, 129, N'A) i-True, ii-True, iii-False', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (466, 129, N'B) i-False, ii-False, iii-True
', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (467, 129, N'C) i-True, ii-False, iii-True', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (468, 129, N'D) i-False, ii-True, iii-True', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (469, 130, N'A) i, ii, iii and iv only', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (470, 130, N'B) i, ii, iii and v only', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (471, 130, N'C) i, ii, iv and v only', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (472, 130, N'D) All i, ii, iii, iv and v', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (473, 131, N'A) selector: pseudo-class{property: value}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (474, 131, N'B) selector.class: pseudo-class{property:value}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (475, 131, N'C) Both A and B', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (476, 131, N'D) None of the above', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (477, 132, N'A) #black{color: #000000;}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (478, 132, N'B) h1 #black{color: #000000;}
', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (479, 132, N'C) #black h2{color: #000000;}', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (480, 132, N'D) All of the above', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (481, 133, N'A) i, iii and iv only', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (482, 133, N'B) i, ii, and v only', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (484, 133, N'C) i, ii, iv and v only', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (485, 133, N'D) All i, ii, iii, iv and v', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (486, 134, N'A) @important', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (487, 134, N'B) #important', 0)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (488, 134, N'C) !important', 1)
INSERT [dbo].[tblAnswer] ([answerID], [questionID], [answerContent], [isRight]) VALUES (489, 134, N'D) !first', 0)
SET IDENTITY_INSERT [dbo].[tblAnswer] OFF
SET IDENTITY_INSERT [dbo].[tblClass] ON 

INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (1, CAST(N'2021-07-21' AS Date), N'Java Class', 1, 25)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (2, CAST(N'2021-07-21' AS Date), N'Web Design Class', 1, 25)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (9, CAST(N'2021-08-03' AS Date), N'Se1432', 1, 25)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (10, CAST(N'2021-08-03' AS Date), N'Se1424', 1, 30)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (11, CAST(N'2021-08-03' AS Date), N'Se1422', 1, 30)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (12, CAST(N'2021-08-04' AS Date), N'SE1429', 1, 23)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (13, CAST(N'2021-08-04' AS Date), N'Se1422', 1, 30)
INSERT [dbo].[tblClass] ([classID], [createDate], [className], [isActive], [numberOfStudent]) VALUES (14, CAST(N'2021-08-05' AS Date), N'Se1522', 1, 30)
SET IDENTITY_INSERT [dbo].[tblClass] OFF
SET IDENTITY_INSERT [dbo].[tblClassCourse] ON 

INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (1, 1, 1)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (2, 2, 3)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (5, 9, 3)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (6, 10, 4)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (7, 11, 4)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (8, 12, 3)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (9, 13, 3)
INSERT [dbo].[tblClassCourse] ([classCourseID], [classID], [courseID]) VALUES (10, 14, 4)
SET IDENTITY_INSERT [dbo].[tblClassCourse] OFF
SET IDENTITY_INSERT [dbo].[tblCourse] ON 

INSERT [dbo].[tblCourse] ([courseID], [courseName], [description], [createUserID], [startAt], [isActive], [createDate]) VALUES (1, N'Java Basic', N'Java Course', N'SE123456', CAST(N'2021-07-21' AS Date), 1, CAST(N'2021-07-21' AS Date))
INSERT [dbo].[tblCourse] ([courseID], [courseName], [description], [createUserID], [startAt], [isActive], [createDate]) VALUES (3, N'Web Design', N'Web Design', N'SE123456', CAST(N'2021-08-04' AS Date), 1, CAST(N'2021-07-21' AS Date))
INSERT [dbo].[tblCourse] ([courseID], [courseName], [description], [createUserID], [startAt], [isActive], [createDate]) VALUES (4, N'PRN192', N'Mon hoc ve C#', N'SE123456', CAST(N'2021-08-06' AS Date), 1, CAST(N'2021-08-03' AS Date))
INSERT [dbo].[tblCourse] ([courseID], [courseName], [description], [createUserID], [startAt], [isActive], [createDate]) VALUES (5, N'PRJ321', N'JAVA WEB', N'SE123456', CAST(N'2021-08-06' AS Date), 0, CAST(N'2021-08-05' AS Date))
SET IDENTITY_INSERT [dbo].[tblCourse] OFF
SET IDENTITY_INSERT [dbo].[tblEvent] ON 

INSERT [dbo].[tblEvent] ([eventID], [eventName], [description], [classCourseID], [createDate], [eventURL], [isActive]) VALUES (2, N'Java Course', N'First Event of Java Course', 1, CAST(N'2021-07-21' AS Date), N'google.com', 0)
INSERT [dbo].[tblEvent] ([eventID], [eventName], [description], [classCourseID], [createDate], [eventURL], [isActive]) VALUES (3, N'Web Design', N'First Event of Web Design Course', 2, CAST(N'2021-07-21' AS Date), N'google.com', 0)
SET IDENTITY_INSERT [dbo].[tblEvent] OFF
SET IDENTITY_INSERT [dbo].[tblLession] ON 

INSERT [dbo].[tblLession] ([lessionID], [subjectID]) VALUES (1, 1)
INSERT [dbo].[tblLession] ([lessionID], [subjectID]) VALUES (2, 2)
INSERT [dbo].[tblLession] ([lessionID], [subjectID]) VALUES (3, 3)
INSERT [dbo].[tblLession] ([lessionID], [subjectID]) VALUES (4, 4)
INSERT [dbo].[tblLession] ([lessionID], [subjectID]) VALUES (5, 5)
INSERT [dbo].[tblLession] ([lessionID], [subjectID]) VALUES (6, 6)
SET IDENTITY_INSERT [dbo].[tblLession] OFF
SET IDENTITY_INSERT [dbo].[tblQuestion] ON 

INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (1, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'1. ________ represents an entity in the real world that can be distinctly identified.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (3, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'2. ________ is a construct that defines objects of the same type.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (4, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'3. An object is an instance of a ________.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (7, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'4. The keyword ________ is required to declare a class.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (9, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'5. ________ is invoked to create an object.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (10, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'6. Given the declaration Circle x = new Circle(), which of the following statement is most accurate?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (12, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'7. The default value for data field of a boolean type, numeric type, object type is ________, respectively.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (14, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'8. A constructor', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (15, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'9. What happens when this statement is executed?
Automobile car = new Automobile(1);', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (16, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'10. If there is no constructor defined by the programmer in the Employee class, what form of constructor, if
any, is implicitly provided?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (17, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'11. If the constructor that follows is in the Employee class, what other form of constructor, if any, is
implicitly provided?
public Employee(String name, int salary) {...}', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (24, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'12. You can determine all the keys in a Map in which of the following ways?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (25, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'13. What keyword is used to prevent an object from being serialized?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (26, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'14. An abstract class can contain methods with declared bodies.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (27, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'15. Select the order of access modifiers from least restrictive to most restrictive.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (28, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'16. Which access modifier allows you to access method calls in libraries not created in Java?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (29, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'17. Which of the following statements are true? (Select all that apply.)', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (30, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'18. The keyword extends refers to what type of relationship?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (31, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'19. Which of the following keywords is used to invoke a method in the parent class?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (32, 1, CAST(N'2021-07-21' AS Date), N'SE111111', N'20. What is the value of x after the following operation is performed?
x = 23 % 4;', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (33, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'1. Which of the following expressions results in a positive value in x?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (35, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'2. Which of the following operations might throw an ArithmeticException?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (36, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'3. Which of the following operations might throw an ArithmeticException?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (37, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'4. Which of the following may appear on the left-hand side of an instanceof operator?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (38, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'5. Which of the following may appear on the right-hand side of an instanceof operator?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (39, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'6. What is -50 >> 1?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (40, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'7. Which of the following statements is true?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (41, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'8. Which modifier or modifiers should be used to denote a variable that should not be written out as part of its class''s persistent state? (Choose the shortest possible answer.)', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (42, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'9. Which of the following statements are true?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (43, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'10. Which of the following statements are true?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (44, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'11. Which of the following statements is correct? (Choose one.)', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (45, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'12. Which of the following statements is true? (Choose one.)', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (46, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'13. Which of the following may legally appear as the new type (between the parentheses) in a cast operation?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (47, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'14. Which of the following may legally appear as the new type (between the parentheses) in a cast operation?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (48, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'15. Suppose the type of xarr is an array of XXX, and the type of yarr is an array of YYY. When is the assignment xarr = yarr; legal?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (49, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'16. When is x & y an int? (Choose one).', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (50, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'17. What are the legal types for whatsMyType?
short s = 10; whatsMyType = !s;', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (51, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'18. When a negative long is cast to a byte, what are the possible values of the result?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (52, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'19. What is the difference between the rules for method-call conversion and the rules for assignment conversion?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (53, 2, CAST(N'2021-07-21' AS Date), N'SE111111', N'20. When is it appropriate to pass a cause to an exception''s constructor?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (54, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'1. Which of the following should always be caught?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (55, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'2. When does an exception''s stack trace get recorded in the exception object?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (56, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'3. When is it appropriate to write code that constructs and throws an error?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (57, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'4. Which of the following may override a method whose signature is void xyz(float f)?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (58, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'5. Suppose x and y are of type TrafficLightState, which is an enum. What is the best way to test whether x and y refer to the same constant?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (59, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'6. Which of the following restrictions apply to anonymous inner classes?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (60, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'7. Which methods return an enum constant''s name?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (61, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'8. Suppose class X contains the following method:
void doSomething(int a, float b) { ... }

Which of the following methods may appear in class Y, which extends X?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (62, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'9. Which of the following methods in the Thread class are deprecated?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (63, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'10. Which of the following statements about threads is true?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (64, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'11. Which of the following statements about the wait() and notify() methods is true?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (65, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'12. How many locks does an object have?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (66, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'13. How do you prevent shared data from being corrupted in a multithreaded environment?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (67, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'14. How can you ensure that multithreaded code does not deadlock?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (68, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'15. Suppose you want to write a class that offers static methods to compute hyperbolic trigonometric functions. You decide to subclass java.lang.Math and provide the new functionality as a set of static methods. Which one statement is true about this strategy?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (69, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'16. Which of the following classes implement java.util.List?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (70, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'17. Suppose you are writing a class that will provide custom serialization. The class implements java.io.Serializable (not java.io.Externalizable). What access mode should the writeObject() method have?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (71, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'18. Suppose you are writing a class that will provide custom deserialization. The class implements java.io.Serializable (not java.io.Externalizable). What access mode should the readObject() method have?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (72, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'19. Suppose class A extends Object; class B extends A; and class C extends B. Of these, only class C implements java.io.Serializable. Which of the following must be true in order to avoid an exception during deserialization of an instance of C?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (73, 3, CAST(N'2021-07-21' AS Date), N'SE111111', N'20. Suppose class A extends Object; Class B extends A; and class C extends B. Of these, only class C implements java.io.Externalizable. Which of the following must be true in order to avoid an exception during deserialization of an instance of C?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (74, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'1. Why so JavaScript and Java have similar name?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (75, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'2. When a user views a page containing a JavaScript program, which machine actually
executes the script?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (77, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'3. ______ JavaScript is also called client-side JavaScript.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (78, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'4. __________ JavaScript is also called server-side', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (79, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'5. What are variables used for in JavaScript Programs?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (80, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'6. _____ JavaScript statements embedded in an HTML page can respond to user events
such as mouse-clicks, form input, and page navigation.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (81, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'7. 
What should appear at the very end of your JavaScript?
The <script LANGUAGE="JavaScript">tag', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (82, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'8. Which of the following can''t be done with client-side JavaScript?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (83, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'9. Which of the following are capabilities of functions in JavaScript?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (84, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'10. Which of the following is not a valid JavaScript variable name?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (85, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'11. ______ tag is an extension to HTML that can enclose any number of JavaScript
statements.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (86, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'12. How does JavaScript store dates in a date object?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (87, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'13. Which of the following attribute can hold the JavaScript version?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (88, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'14. What is the correct JavaScript syntax to write "Hello World"?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (89, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'15. Which of the following way can be used to indicate the LANGUAGE attribute?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (90, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'16. Inside which HTML element do we put the JavaScript?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (91, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'17. What is the correct syntax for referring to an external script called " abc.js"?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (92, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'18. Which types of image maps can be used with JavaScript?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (93, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'19. Which of the following navigator object properties is the same in both Netscape and
IE?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (94, 6, CAST(N'2021-07-21' AS Date), N'SE111111', N'20. Which is the correct way to write a JavaScript array?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (95, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'1. What does the <noscript> tag do?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (96, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'2. If para1 is the DOM object for a paragraph, what is the correct syntax to change the
text within the paragraph?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (97, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'3. JavaScript entities start with _______ and end with _________.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (98, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'4. Which of the following best describes JavaScript?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (99, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'
5. Choose the server-side JavaScript object?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (100, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'
6. Choose the client-side JavaScript object?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (101, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'7. Which of the following is not considered a JavaScript operator?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (102, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'8. ______method evaluates a string of JavaScript code in the context of the specified
object.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (103, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'9. Which of the following event fires when the form element loses the focus: <button>,
<input>, <label>, <select>, <textarea>?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (104, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'10. The syntax of Eval is ________________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (105, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'
11. JavaScript is interpreted by _________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (106, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'12. Using _______ statement is how you test for a specific condition.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (107, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'13. Which of the following is the structure of an if statement?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (108, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'14. How to create a Date object in JavaScript?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (109, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'15. The _______ method of an Array object adds and/or removes elements from an
array.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (110, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'16. To set up the window to capture all Click events, we use which of the following
statement?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (111, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'
17. Which tag(s) can handle mouse events in Netscape?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (112, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'18. ____________ is the tainted property of a window object.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (113, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'19. To enable data tainting, the end user sets the _________ environment variable.', 1)
GO
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (114, 5, CAST(N'2021-07-21' AS Date), N'SE111111', N'20. In JavaScript, _________ is an object of the target language data type that encloses
an object of the source language.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (115, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'
1. When a JavaScript object is sent to Java, the runtime engine creates a Java wrapper
of type ___________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (116, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'2. _______ class provides an interface for invoking JavaScript methods and examining
JavaScript properties.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (117, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'3. _________ is a wrapped Java array, accessed from within JavaScript code.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (118, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'4. A ________ object is a reference to one of the classes in a Java package, such as
netscape.javascript .', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (119, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'
5. The JavaScript exception is available to the Java code as an instance of __________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (120, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'6. To automatically open the console when a JavaScript error occurs which of the
following is added to prefs.js?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (121, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'7. To open a dialog box each time an error occurs, which of the following is added to
prefs.js?', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (122, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'8. The syntax of a blur method in a button object is ______________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (123, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'9. The syntax of capture events method for document object is ______________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (124, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'10. The syntax of close method for document object is ______________', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (125, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'11. The following syntax to set margin around a paragraph will make- p{margin:10px 2%}', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (126, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'12. The ...................... property allows you to control the shape or style of bullet point in the case of unordered lists, and the style of numbering characters in ordered list.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (127, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'13. The ......................... property allows to specify the distance between the list and the text relating to the list.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (128, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'14. The ...................... property allows to specify how much space appear between the content of an element and it''s border.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (129, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'15. State True or False for CSS outlines properties.

i) An outline does take up space

ii) Outline do not have to be rectangular.

iii) Outline is always the same on all sides.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (130, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'
16. The overflow property in CSS can take one of the following values.

i) visible ii) hidden iii) scroll iv) non-scroll v) auto', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (131, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'17. Which of the following is/are the valid syntax for CSS pseudo classes.', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (132, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'18. The valid examples of ID selectors is/are', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (133, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'19. Which of the following is/are the possible values of CSS pseudo element property?

i) : first line ii) : last-line iii) : before iv) : after v) : between', 1)
INSERT [dbo].[tblQuestion] ([questionID], [subjectID], [createDate], [createUserID], [questionContent], [isActive]) VALUES (134, 4, CAST(N'2021-07-21' AS Date), N'SE111111', N'20. 
The ............... rule is used to make sure that the property always be applied whether another property appears in CSS.', 1)
SET IDENTITY_INSERT [dbo].[tblQuestion] OFF
SET IDENTITY_INSERT [dbo].[tblQuiz] ON 

INSERT [dbo].[tblQuiz] ([quizID], [subjectID], [quizName], [quizNum], [quizTime], [startAt], [endAt], [isActive]) VALUES (1, 1, N'Java Basic Quiz', 20, N'30', CAST(N'2021-07-21 00:00:00.000' AS DateTime), CAST(N'2022-07-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tblQuiz] ([quizID], [subjectID], [quizName], [quizNum], [quizTime], [startAt], [endAt], [isActive]) VALUES (4, 2, N'Object Orientation Quiz', 20, N'30', CAST(N'2021-07-21 00:00:00.000' AS DateTime), CAST(N'2022-07-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tblQuiz] ([quizID], [subjectID], [quizName], [quizNum], [quizTime], [startAt], [endAt], [isActive]) VALUES (5, 3, N'Final Quiz', 20, N'30', CAST(N'2021-07-21 00:00:00.000' AS DateTime), CAST(N'2022-07-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tblQuiz] ([quizID], [subjectID], [quizName], [quizNum], [quizTime], [startAt], [endAt], [isActive]) VALUES (7, 4, N'HTML&CSS Quiz', 20, N'30', CAST(N'2021-07-21 00:00:00.000' AS DateTime), CAST(N'2022-07-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tblQuiz] ([quizID], [subjectID], [quizName], [quizNum], [quizTime], [startAt], [endAt], [isActive]) VALUES (8, 5, N'JavaScript Quiz', 20, N'30', CAST(N'2021-07-21 00:00:00.000' AS DateTime), CAST(N'2022-07-21 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[tblQuiz] ([quizID], [subjectID], [quizName], [quizNum], [quizTime], [startAt], [endAt], [isActive]) VALUES (9, 6, N'Final Quiz', 20, N'30', CAST(N'2021-07-21 00:00:00.000' AS DateTime), CAST(N'2022-07-21 00:00:00.000' AS DateTime), 1)
SET IDENTITY_INSERT [dbo].[tblQuiz] OFF
SET IDENTITY_INSERT [dbo].[tblRole] ON 

INSERT [dbo].[tblRole] ([roleID], [roleName]) VALUES (1, N'admin')
INSERT [dbo].[tblRole] ([roleID], [roleName]) VALUES (2, N'teacher')
INSERT [dbo].[tblRole] ([roleID], [roleName]) VALUES (3, N'student')
SET IDENTITY_INSERT [dbo].[tblRole] OFF
INSERT [dbo].[tblStudentCourse] ([studentJoinID], [classCourseID], [courseMark], [joinDate]) VALUES (N'SE130005', 7, 0, CAST(N'2021-07-26' AS Date))
INSERT [dbo].[tblStudentCourse] ([studentJoinID], [classCourseID], [courseMark], [joinDate]) VALUES (N'SE142313', 10, 0, CAST(N'2021-08-05' AS Date))
SET IDENTITY_INSERT [dbo].[tblSubject] ON 

INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (1, N'The Very Basic of Java', 1, 0)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (2, N'Understanding Object Orientation', 1, 1)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (3, N'Putting it Together', 1, 1)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (4, N'What is HTML and CSS?', 3, 1)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (5, N'About JavaScript', 3, 0)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (6, N'Putting it Together', 3, 0)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (7, N'Chapter 1: Servlet', 4, 1)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (8, N'Chapter 2: JDBC and Dynamic Connection', 4, 1)
INSERT [dbo].[tblSubject] ([subjectID], [subjectName], [courseID], [isActive]) VALUES (9, N'Chapter 1: Servlet', 5, 1)
SET IDENTITY_INSERT [dbo].[tblSubject] OFF
INSERT [dbo].[tblUser] ([userID], [roleID], [fullName], [email], [phoneNum], [password], [createDate]) VALUES (N'SE111111', 1, N'Nguyen Van Admin', N'NVAdmin@gmail.com', N'0123456789', N'admin', CAST(N'2021-07-19' AS Date))
INSERT [dbo].[tblUser] ([userID], [roleID], [fullName], [email], [phoneNum], [password], [createDate]) VALUES (N'SE123456', 2, N'Nguyen Van Teacher', N'NVTeacher@gmail.com', N'0548248489', N'teacher', CAST(N'2021-07-19' AS Date))
INSERT [dbo].[tblUser] ([userID], [roleID], [fullName], [email], [phoneNum], [password], [createDate]) VALUES (N'SE130005', 3, N'Tran Thi Student', N'TTStudent@gmail.com', N'0252929418', N'student', CAST(N'2021-07-19' AS Date))
INSERT [dbo].[tblUser] ([userID], [roleID], [fullName], [email], [phoneNum], [password], [createDate]) VALUES (N'SE142313', 3, N'Nguyen An', N'Abc@gmail.com', N'1828103123', N'student', CAST(N'2021-07-19' AS Date))
ALTER TABLE [dbo].[tblAnswer]  WITH CHECK ADD  CONSTRAINT [FK_tblAnswer_tblQuestion] FOREIGN KEY([questionID])
REFERENCES [dbo].[tblQuestion] ([questionID])
GO
ALTER TABLE [dbo].[tblAnswer] CHECK CONSTRAINT [FK_tblAnswer_tblQuestion]
GO
ALTER TABLE [dbo].[tblClassCourse]  WITH CHECK ADD  CONSTRAINT [FK_tblClassCourse_tblClass] FOREIGN KEY([classID])
REFERENCES [dbo].[tblClass] ([classID])
GO
ALTER TABLE [dbo].[tblClassCourse] CHECK CONSTRAINT [FK_tblClassCourse_tblClass]
GO
ALTER TABLE [dbo].[tblClassCourse]  WITH CHECK ADD  CONSTRAINT [FK_tblClassCourse_tblCourse] FOREIGN KEY([courseID])
REFERENCES [dbo].[tblCourse] ([courseID])
GO
ALTER TABLE [dbo].[tblClassCourse] CHECK CONSTRAINT [FK_tblClassCourse_tblCourse]
GO
ALTER TABLE [dbo].[tblCourse]  WITH CHECK ADD  CONSTRAINT [FK_tblCourse_tblUser] FOREIGN KEY([createUserID])
REFERENCES [dbo].[tblUser] ([userID])
GO
ALTER TABLE [dbo].[tblCourse] CHECK CONSTRAINT [FK_tblCourse_tblUser]
GO
ALTER TABLE [dbo].[tblEvent]  WITH CHECK ADD  CONSTRAINT [FK_tblEvent_tblClassCourse] FOREIGN KEY([classCourseID])
REFERENCES [dbo].[tblClassCourse] ([classCourseID])
GO
ALTER TABLE [dbo].[tblEvent] CHECK CONSTRAINT [FK_tblEvent_tblClassCourse]
GO
ALTER TABLE [dbo].[tblFeedback]  WITH CHECK ADD  CONSTRAINT [FK_tblFeedback_tblClassCourse] FOREIGN KEY([classCourseID])
REFERENCES [dbo].[tblClassCourse] ([classCourseID])
GO
ALTER TABLE [dbo].[tblFeedback] CHECK CONSTRAINT [FK_tblFeedback_tblClassCourse]
GO
ALTER TABLE [dbo].[tblFile]  WITH CHECK ADD  CONSTRAINT [FK_tblFile_tblLession] FOREIGN KEY([lessionID])
REFERENCES [dbo].[tblLession] ([lessionID])
GO
ALTER TABLE [dbo].[tblFile] CHECK CONSTRAINT [FK_tblFile_tblLession]
GO
ALTER TABLE [dbo].[tblLession]  WITH CHECK ADD  CONSTRAINT [FK_tblLession_tblSubject] FOREIGN KEY([subjectID])
REFERENCES [dbo].[tblSubject] ([subjectID])
GO
ALTER TABLE [dbo].[tblLession] CHECK CONSTRAINT [FK_tblLession_tblSubject]
GO
ALTER TABLE [dbo].[tblQuestion]  WITH CHECK ADD  CONSTRAINT [FK_tblQuestion_tblSubject] FOREIGN KEY([subjectID])
REFERENCES [dbo].[tblSubject] ([subjectID])
GO
ALTER TABLE [dbo].[tblQuestion] CHECK CONSTRAINT [FK_tblQuestion_tblSubject]
GO
ALTER TABLE [dbo].[tblQuiz]  WITH CHECK ADD  CONSTRAINT [FK_tblQuiz_tblSubject1] FOREIGN KEY([subjectID])
REFERENCES [dbo].[tblSubject] ([subjectID])
GO
ALTER TABLE [dbo].[tblQuiz] CHECK CONSTRAINT [FK_tblQuiz_tblSubject1]
GO
ALTER TABLE [dbo].[tblStudentAnswer]  WITH CHECK ADD  CONSTRAINT [FK_tblStudentAnswer_tblAnswer] FOREIGN KEY([answerID])
REFERENCES [dbo].[tblAnswer] ([answerID])
GO
ALTER TABLE [dbo].[tblStudentAnswer] CHECK CONSTRAINT [FK_tblStudentAnswer_tblAnswer]
GO
ALTER TABLE [dbo].[tblStudentAnswer]  WITH CHECK ADD  CONSTRAINT [FK_tblStudentAnswer_tblQuiz] FOREIGN KEY([quizID])
REFERENCES [dbo].[tblQuiz] ([quizID])
GO
ALTER TABLE [dbo].[tblStudentAnswer] CHECK CONSTRAINT [FK_tblStudentAnswer_tblQuiz]
GO
ALTER TABLE [dbo].[tblStudentAnswer]  WITH CHECK ADD  CONSTRAINT [FK_tblStudentAnswer_tblStudentCourse] FOREIGN KEY([studentJoinID])
REFERENCES [dbo].[tblStudentCourse] ([studentJoinID])
GO
ALTER TABLE [dbo].[tblStudentAnswer] CHECK CONSTRAINT [FK_tblStudentAnswer_tblStudentCourse]
GO
ALTER TABLE [dbo].[tblStudentCourse]  WITH CHECK ADD  CONSTRAINT [FK_tblStudentCourse_tblClassCourse] FOREIGN KEY([classCourseID])
REFERENCES [dbo].[tblClassCourse] ([classCourseID])
GO
ALTER TABLE [dbo].[tblStudentCourse] CHECK CONSTRAINT [FK_tblStudentCourse_tblClassCourse]
GO
ALTER TABLE [dbo].[tblSubject]  WITH CHECK ADD  CONSTRAINT [FK_tblSubject_tblCourse] FOREIGN KEY([courseID])
REFERENCES [dbo].[tblCourse] ([courseID])
GO
ALTER TABLE [dbo].[tblSubject] CHECK CONSTRAINT [FK_tblSubject_tblCourse]
GO
ALTER TABLE [dbo].[tblUser]  WITH CHECK ADD  CONSTRAINT [FK_tblUser_tblRole] FOREIGN KEY([roleID])
REFERENCES [dbo].[tblRole] ([roleID])
GO
ALTER TABLE [dbo].[tblUser] CHECK CONSTRAINT [FK_tblUser_tblRole]
GO
