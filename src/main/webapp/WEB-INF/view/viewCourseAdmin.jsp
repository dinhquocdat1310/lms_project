<%-- 
    Document   : viewCourseAdmin
    Created on : Aug 2, 2021, 4:10:39 PM
    Author     : Asus
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- BS -->
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
            />
        <!-- CDN Font awesome -->
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            />
        <!-- Main -->
        <link rel="stylesheet" href="./resources/css/style.css">
        <title>Canvas</title>
    </head>
    <body>
        <section class="home__page">
            <div class="home__content">
                <div class="home__left">
                    <img src="../IMG/canvas-min.jpg" alt="" srcset="" />
                    <div class="select__Info">
                        <a href="profile.html">
                            <i class="fas fa-user-circle"></i>
                            <p class="select__link">Account</p>
                        </a>
                    </div>
                    <div class="select__Info">
                        <a href="viewCourseAdmin">
                            <i class="fas fa-tachometer-alt"></i>
                            <p class="select__link">Dashboard</p>
                        </a>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#course"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-book"></i>
                        <p>Courses</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-calendar-alt"></i>
                        <p>Calendar</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-envelope-open-text"></i>
                        <p>Inbox</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="far fa-clock"></i>
                        <p>History</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-info-circle"></i>
                        <p>Help</p>
                    </div>
                </div>
                <div class="home__middle__listProduct">
                    <div style="margin-bottom: 100px" class="middle__title__listProduct">
                        <div class="middle__header">
                            <h2>All Courses</h2>
                            <div>                                
                                <a class="btn__link" href="listClass">
                                    <button class="btn__viewStudent btn btn-outline-secondary">
                                        View List Class                                 
                                    </button>
                                </a>
                            </div>
                        </div>                        
                    </div>          
                    <c:if test="${not empty listCourse}">
                        <div class="middle__detail__listProduct">

                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Course</th>                  
                                        <th scope="col">Start At</th>
                                        <th scope="col">Description</th>
                                        <th scope="col">Published</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="tb_style">                                    
                                    <c:forEach var="dto" items="${listCourse}">
                                        <tr>
                                            <th scope="row">                                            
                                                <c:if test="${dto.isActive eq 'true'}">
                                                    <i data-bs-toggle="tooltip" data-bs-placement="bottom" title="Published" class="fa fa-star" aria-hidden="true"></i>
                                                </c:if>
                                                <c:if test="${dto.isActive eq 'false'}">
                                                    <i data-bs-toggle="tooltip" data-bs-placement="bottom" title="Unpublish" class="far fa-star"></i>
                                                </c:if>
                                            </th>
                                            <td>${dto.courseName}</td>                  
                                            <td>${dto.startAt}</td>
                                            <td>${dto.description}</td>
                                            <td class="text-danger">
                                                <c:if test="${dto.isActive eq 'true'}">
                                                    Yes
                                                </c:if>
                                                <c:if test="${dto.isActive eq 'false'}">
                                                    No
                                                </c:if>
                                            </td>
                                            <td>
<!--                                            <a href="subjectCreate?courseID=${dto.courseID}"><button type="submit" class="btn btn-secondary fill">Add Subject</button></a>
                                                <a href="updateCourse?courseID=${dto.courseID}"><button type="submit" class="btn btn-secondary fill">Edit Course</button></a>-->
                                                <a href="addClass?courseID=${dto.courseID}"><button type="submit" class="btn btn-secondary fill">Add Class</button></a>                                                    
                                            </td>                                            
                                        </tr>
                                    </c:forEach>                                  
                                </tbody>
                            </table>                                                          
                        </div>
                    </c:if>
                    <c:if test="${empty listCourse}">
                        <h2>You don't have any course. Please click " + Create " to add new Course.</h2>
                    </c:if>          
                </div>
            </div>
            <div
                class="offcanvas offcanvas-start"
                data-bs-scroll="true"
                data-bs-backdrop="false"
                tabindex="-1"
                id="course"
                aria-labelledby="offcanvasScrollingLabel"
                >
                <div class="offcanvas-header">
                    <h4 class="offcanvas-title" id="offcanvasScrollingLabel">Courses</h4>
                    <button
                        type="button"
                        class="btn-close text-reset"
                        data-bs-dismiss="offcanvas"
                        aria-label="Close"
                        ></button>
                </div>
                <div class="offcanvas-body">
                    <div class="course__title">
                        <h5>Published Courses</h5>
                    </div>
                    <div class="course__item">
                        <ul class="list_item">
                            <c:forEach var="dtoCourse" items="${listCourseActive}">
                                <li><a href="subjectCreate?courseID=${dtoCourse.courseID}">${dtoCourse.courseName}</a></li>                                
                                </c:forEach>                                                
                        </ul>                       
                    </div>
                    <div class="course__title">
                        <h5>Unpublished Courses</h5>
                    </div>
                    <div class="course__item">
                        <ul class="list_item">
                            <c:forEach var="dtoUCourse" items="${listCourseInActive}">                                
                                <li><a href="subjectCreate?courseID=${dtoUCourse.courseID}">${dtoUCourse.courseName}</a></li>                                
                                </c:forEach>
                            <hr />
                            <a href="listCourse">All Course</a>
                        </ul>
                        <p>
                            Welcome to your courses! To customize the list of courses, click
                            on the "All Courses" link and star the courses to display.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>
    </body>
</html>
