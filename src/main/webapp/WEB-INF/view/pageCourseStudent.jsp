<%-- 
    Document   : pageCourseStudent
    Created on : Aug 1, 2021, 5:07:03 PM
    Author     : Asus
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- BS -->
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
            />
        <!-- CDN Font awesome -->
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            />
        <!-- Main -->
        <link rel="stylesheet" href="./resources/css/style.css">
        <link rel="stylesheet" href="./resources/css/date.css">
        <title>Canvas</title>
    </head>
    <body>
        <section class="home__page">
            <div class="home__content">
                <div class="home__left">
                    <img src="../IMG/canvas-min.jpg" alt="" srcset="" />
                    <div class="select__Info">
                        <i class="fas fa-user-circle"></i>
                        <p>Account</p>
                    </div>
                    <div class="select__Info">
                        <a href="pageCourseStudent">
                            <i class="fas fa-tachometer-alt"></i>
                            <p class="select__link">Dashboard</p>
                        </a>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#course"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-book"></i>
                        <p>Courses</p>
                    </div>
                    <div class="select__Info">
                        <i class="fas fa-calendar-alt"></i>
                        <p>Calendar</p>
                    </div>
                    <div class="select__Info">
                        <i class="fas fa-envelope-open-text"></i>
                        <p>Inbox</p>
                    </div>
                    <div class="select__Info">
                        <i class="far fa-clock"></i>
                        <p>History</p>
                    </div>
                    <div class="select__Info">
                        <i class="fas fa-info-circle"></i>
                        <p>Help</p>
                    </div>
                </div>
                <div class="home__middle">
                    <div class="middle__title">
                        <h2>Dashboard</h2>
                    </div>
                    <div class="middle__content">
                        <h3>All Courses</h3>
                    </div>
                    <div class="middle__detail">
                        <c:forEach var="dtoCourse" items="${listCourseActive}">
                            <div class="card" style="width: 18rem">
                                <div class="card-img-top"></div>
                                <div class="card-body">
                                    <h5 class="card-title">${dtoCourse.courseName} - Teacher: <c:forEach var="dtoTeacher" items="${listTeacher}">
                                            <c:if test="${dtoTeacher.userID eq dtoCourse.createUserID.userID}">                                                
                                                ${dtoTeacher.fullName}
                                            </c:if>
                                        </c:forEach>
                                    </h5>                                            
                                    <p class="card-text">
                                        ${dtoCourse.description}
                                    </p>
                                    <a href="joinCourse?txtCourseName=${dtoCourse.courseName}&txtCourseID=${dtoCourse.courseID}&txtTeacherID=${dtoCourse.createUserID.userID}" class="btn btn-info">Join Course</a>
                                </div> 
                            </div>
                        </c:forEach>                                      
                    </div>        
                </div>                
            </div>
            <div
                class="offcanvas offcanvas-start"
                data-bs-scroll="true"
                data-bs-backdrop="false"
                tabindex="-1"
                id="course"
                aria-labelledby="offcanvasScrollingLabel"
                >
                <div class="offcanvas-header">
                    <h4 class="offcanvas-title" id="offcanvasScrollingLabel">Courses</h4>
                    <button
                        type="button"
                        class="btn-close text-reset"
                        data-bs-dismiss="offcanvas"
                        aria-label="Close"
                        ></button>
                </div>
                <div class="offcanvas-body">
                    <div class="course__title">
                        <h5>Published Courses</h5>
                    </div>
                    <div class="course__item">
                        <ul class="list_item">
                            <c:forEach var="dtoCourse" items="${listCourseActive}">
                                <li><a href="#">${dtoCourse.courseName}</a></li>                                
                                </c:forEach>                                                
                        </ul>
                        <p>
                            Welcome to your courses! To customize the list of courses, click
                            on the "All Courses" link and star the courses to display.
                        </p>                   
                    </div>
                </div>
        </section>

        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>
    </body>
</html>

