<%-- 
    Document   : login
    Created on : Aug 5, 2021, 3:50:56 AM
    Author     : Asus
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./resources/css/bslogin.css">
        <title>Login Page</title>
    </head>
    <body>       
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->
                <h2 class="active"> Sign In </h2>                 

                <!-- Login Form -->
                <form action="checkLogin">
                    <input type="text" id="login" class="fadeIn second" name="txtUserID" placeholder="userID">
                    <input type="text" id="password" class="fadeIn third" name="txtPassword" placeholder="password">
                    <p style="color: red">${ERROR_LOGIN}</p>
                    <button class="fadeIn fourth" type="submit">Login</button>                                                        
                </form>

                <!-- Remind Passowrd -->
                <div id="formFooter">
                    <a class="underlineHover" href="#">Forgot Password?</a>
                </div>

            </div>
        </div>
    </body>
</html>
