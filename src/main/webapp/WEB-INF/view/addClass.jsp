<%-- 
    Document   : addClass
    Created on : Aug 2, 2021, 1:19:42 PM
    Author     : Asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- BS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!-- CDN Font awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />

    <!-- Lession CSS -->
    <link rel="stylesheet" href="./resources/css/addClass.css"/>
    <!-- Main -->
    <link rel="stylesheet" href="./resources/css/style.css" />
    <title>Canvas</title>
  </head>
  <body>
    <!-- Thanh Navigation -->
    <section class="home__page">
      <div class="home__content">
        <div class="home__left">
          <img src="../IMG/canvas-min.jpg" alt="" srcset="" />
          <div class="select__Info">
            <a href="./profile.html">
              <i class="fas fa-user-circle"></i>
              <p class="select__link">Account</p>
            </a>
          </div>
          <div class="select__Info">
            <a href="pageCourseTeacher">
              <i class="fas fa-tachometer-alt"></i>
              <p class="select__link">Dashboard</p>
            </a>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#course"
            aria-controls="offcanvasScrolling"
          >
            <i class="fas fa-book"></i>
            <p>Courses</p>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <a href="./calendar.html">
              <i class="fas fa-calendar-alt"></i>
              <p>Calendar</p>
            </a>           
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <i class="fas fa-envelope-open-text"></i>
            <p>Inbox</p>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <i class="far fa-clock"></i>
            <p>History</p>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <i class="fas fa-info-circle"></i>
            <p>Help</p>
          </div>
        </div> 
      <div class="course__middle">
        <div class="middle__title" style="margin-bottom: 0px ;">
          <h2>
            Course Name
          </h2>          
        </div>

      <!-- DashBoard Path -->
        <div class="dashboard_path">
          <div class="path">
            <a href="index.html">Dashboard</a>              
            <a href="#"><i class="fas fa-angle-double-right"></i>Cource Name</a>
            <a href="#"><i class="fas fa-angle-double-right"></i>Create new class</a>
          </div>          
          <div class="search-box">
            <div>
              <input placeholder="Search courses" type="text" name="search" autocomplete="off">
            </div>           
            <div>
              <button class="btn-search">
                <i class="fas fa-search"></i>
              </button>
            </div>            
          </div>
        </div>
      <hr style="width: 1320px; margin: 60px 0px 20px 50px;">

      <!-- Add Class -->
      <div class="main_Container">
        <script language="javascript" src="./resources/js/validate.js"></script>
        <form action="createClass" class="add_form" method="POST" onsubmit="return validate()">
          <h3>Create New Class</h3> 
          <label>Course Name:</label>
          <input type="text" name="txtCourseName" value="${COURSE_ITEM.courseName}" disabled=""><br>
          <input type="hidden" name="txtCourseID" value="${COURSE_ITEM.courseID}" />
          <label>Class Name:</label>
          <input type="text" name="txtClassName" value="" id="className" required=""><br>
          <label>Number of Students:</label>
          <input type="text" value="" name="txtNumberOfStudents" id="numberOfStudents"><br>                 
          <br>
          <button type="submit" class="submit">Create</button>
        </form>
      </div>
      <div
        class="offcanvas offcanvas-start"
        data-bs-scroll="true"
        data-bs-backdrop="false"
        tabindex="-1"
        id="course"
        aria-labelledby="offcanvasScrollingLabel"
      >
        <div class="offcanvas-header">
          <h4 class="offcanvas-title" id="offcanvasScrollingLabel">Courses</h4>
          <button
            type="button"
            class="btn-close text-reset"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          ></button>
        </div>
        <div class="offcanvas-body">
          <div class="course__title">
            <h5>Published Courses</h5>
          </div>
          <div class="course__item">
            <ul class="list_item">
              <li><a href="#">Demo Canvas</a></li>
              <li><a href="#">Test Course</a></li>
              <hr>             
              <a href="./listCourse.html">All Course</a>
            </ul>
            <p>
              Welcome to your courses! To customize the list of courses, click on
              the "All Courses" link and star the courses to display.
            </p>                    
          </div>        
        </div>        
      </div>
    </section>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
      integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
      integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
      crossorigin="anonymous"
    ></script>
  </body>
</html>

