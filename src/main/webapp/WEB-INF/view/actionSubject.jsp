<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- BS -->
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
            />
        <!-- CDN Font awesome -->
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            />
        <!-- Main -->
        <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
        <!-- Main -->
        <link rel="stylesheet" href="./resources/css/style.css">
        <link rel="stylesheet" href="./resources/css/date.css">
        <title>Canvas</title>
    </head>
    <body>
        <section class="home__page">
            <div class="home__content">
                <div class="home__left">
                    <img src="../IMG/canvas-min.jpg" alt="" srcset="" />
                    <div class="select__Info">
                        <a href="./profile.html">
                            <i class="fas fa-user-circle"></i>
                            <p class="select__link">Account</p>
                        </a>
                    </div>
                    <div class="select__Info">
                        <a href="./index.html">
                            <i class="fas fa-tachometer-alt"></i>
                            <p class="select__link">Dashboard</p>
                        </a>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#course"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-book"></i>
                        <p>Courses</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <a href="./calendar.html">
                            <i class="fas fa-calendar-alt"></i>
                            <p>Calendar</p>
                        </a>           
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-envelope-open-text"></i>
                        <p>Inbox</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="far fa-clock"></i>
                        <p>History</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-info-circle"></i>
                        <p>Help</p>
                    </div>
                </div>        
                <div class="home__middle">
                    <div class="middle__title">
                        <h2>Add Subject</h2>          
                    </div>        
                    <div class="middle__detail">                     
                        <div>
                            <a href="finishAdd">
                                <button type="button" class="btn1 btn btn-outline-secondary">
                                    Return
                                </button>
                            </a>
                        </div>          
                    </div>
                    
                    <!--ITEM_SUBJECT-->
                    <table class="table" style="margin-left: 100px; margin-top: 50px; text-align: center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Subject Name</th>
                                <th scope="col">Active</th>   
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:if test="${not empty ITSJ}">   
                            <form action="updateSubject">
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <input name="txtSubjectNameEdit" class="font-weight-bold multisteps-form__input form-control" value="${ITSJ.subjectName}" type="text">
                                        <input type="hidden" name="txtSubjectIDEdit" value="${ITSJ.subjectID}"/>
                                        <input type="hidden" name="txtCourseID" value="${COURSEID}"/>
                                    </td>
                                    <td>                                       
                                        <c:if test="${ITSJ.isActive eq 'true'}">
                                            Yes
                                        </c:if>
                                        <c:if test="${ITSJ.isActive eq 'false'}">
                                            No
                                        </c:if>
                                    </td>
                                    <td>                                                                                                                                                                                                                                            
                                        <div class="form-check form-check-inline" style="margin-left: 10px; margin-right: 20px">
                                            <input class="form-check-input" type="radio" name="txtIsActiveSubject" id="Publish" value="true" <c:if test="${ITSJ.isActive eq 'true'}"> checked="checked"</c:if> >
                                                <label style="margin-left: 5px" class="form-check-label" for="Publish">Publish Subject</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="txtIsActiveSubject" id="Unpublish" value="false" <c:if test="${ITSJ.isActive eq 'false'}"> checked="checked"</c:if> >
                                                <label style="margin-left: 5px" class="form-check-label" for="Unpublish">Unpublish Subject</label>
                                            </div>
                                        </td>                                        
                                    </tr>                                    
                                    <button type="submit" class="btn btn-secondary">Save</button>                                   
                                </form>
                        </c:if>
                        <c:if test="${empty ITSJ}">
                            <h2>
                                Nothing.
                            </h2>
                        </c:if>
                        </tbody>
                    </table>
                </div>
        </section>

        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>
        <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js'></script>
        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script><script  src="./resources/js/addCourse.js"></script>
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script><script  src="./resources/js/date.js"></script>
    </body>
</html>
