<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- BS -->
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
            />
        <!-- CDN Font awesome -->
        <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
            />
        <!-- Main -->
        <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
        <!-- Main -->
        <link rel="stylesheet" href="./resources/css/style.css">
        <link rel="stylesheet" href="./resources/css/date.css">
        <title>Canvas</title>
    </head>
    <body>
        <section class="home__page">
            <div class="home__content">
                <div class="home__left">
                    <img src="../IMG/canvas-min.jpg" alt="" srcset="" />
                    <div class="select__Info">
                        <a href="./profile.html">
                            <i class="fas fa-user-circle"></i>
                            <p class="select__link">Account</p>
                        </a>
                    </div>
                    <div class="select__Info">
                        <a href="pageCourseTeacher">
                            <i class="fas fa-tachometer-alt"></i>
                            <p class="select__link">Dashboard</p>
                        </a>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#course"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-book"></i>
                        <p>Courses</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <a href="./calendar.html">
                            <i class="fas fa-calendar-alt"></i>
                            <p>Calendar</p>
                        </a>           
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-envelope-open-text"></i>
                        <p>Inbox</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="far fa-clock"></i>
                        <p>History</p>
                    </div>
                    <div
                        class="select__Info"
                        data-bs-toggle="offcanvas"
                        data-bs-target="#offcanvasScrolling"
                        aria-controls="offcanvasScrolling"
                        >
                        <i class="fas fa-info-circle"></i>
                        <p>Help</p>
                    </div>
                </div>        
                <div class="home__middle">
                    <div class="middle__title">
                        <h2>Add Subject</h2>          
                    </div>        
                    <div class="middle__detail">                     
                        <div>
                            <a href="finishAdd">
                                <button type="button" class="btn1 btn btn-outline-secondary">
                                    Return
                                </button>
                            </a>
                        </div>          
                    </div>          
                    <div class="container overflow-hidden">
                        <div class="multisteps-form">            
                            <div class="row">
                                <div class="col-12">
                                    <div class="multisteps-form__progress">
                                        <button class="multisteps-form__progress-btn js-active" type="button" style="display: none;" title="Subject">Subject</button>
                                    </div>
                                </div>
                            </div>            
                            <div class="row">
                                <div class="col-12">
                                    <div class="multisteps-form__form" style="height: 329px;">
                                        <c:set var="dto_subInfo" value="${listSubject}"/>
                                        <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                                            <h3 class="multisteps-form__title">Subject</h3>
                                            <div class="multisteps-form__content">
                                                <div class="row">
                                                    <div class="col-12 col-md-4 mt-4">
                                                        <div class="card" style="width: 18rem;"></div>
                                                    </div>
                                                    <div class="col-12 col-md-4 mt-4">
                                                        <div class="card" style="width: 28rem;">
                                                            <div class="card-header">
                                                                <input class="font-weight-bold multisteps-form__input form-control" value="${course_Item.courseName}" type="text" readonly="readonly">
                                                            </div>
                                                            <c:if test="${not empty listSubject}">
                                                                <c:forEach var="dto_Subject" items="${listSubject}">
                                                                    <form action="actionEditSubject">
                                                                        <ul class="list-group list-group-flush">
                                                                            <li class="list-group-item">
                                                                                <c:if test="${dto_Subject.isActive eq 'true'}">
                                                                                    <i style="margin-right: 5px" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Published" class="fa fa-star" aria-hidden="true"></i>
                                                                                </c:if>
                                                                                <c:if test="${dto_Subject.isActive eq 'false'}">
                                                                                    <i style="margin-right: 5px" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Unpublish" class="far fa-star"></i>
                                                                                </c:if>
                                                                                ${dto_Subject.subjectName}                                                                                
                                                                                <input type="hidden" name="txtSubjectID" value="${dto_Subject.subjectID}" />
                                                                                <input type="hidden" name="txtCourseID" value="${course_Item.courseID}" />                                                                                
                                                                                <a style="text-decoration: none; float: right;font-size: 1.5rem; font-weight: 700;line-height: 1;color: #000;text-shadow: 0 1px 0 #fff;opacity: .5;" href="actionEditSubject">                                                                                
                                                                                    <button type="submit" class="btn btn-outline-secondary">Action</button>   
                                                                                </a>                                                                                                                                                         
                                                                            </li>                                                                        
                                                                    </form>
                                                                </c:forEach>
                                                            </c:if>
                                                            <c:if test="${empty listSubject}">
                                                                <li class="list-group-item">
                                                                    Nothing.                           
                                                                </li>                   
                                                            </c:if>
                                                            <li class="list-group-item add-module"><a data-toggle="modal" data-target="#XYZ${course_Item.courseID}" href="#">+ Add Subject</a></li>                
                                                            <div class="row">
                                                                <div class="button-row d-flex mt-4 col-12">
                                                                    <a href="finishAdd"><button class="btn btn-secondary btn-finish" type="button" style="align-items: center;" title="Next">Finish</button></a>
                                                                </div>
                                                            </div>                                                                                                                                                                                                               
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>                                                        
                                            </div>
                                        </div>                                        
                                    </div>
                                    <form action="addNewSubject">
                                        <div class="modal fade" id="XYZ${course_Item.courseID}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Enter a Subject Name</h5>      
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <input name="txtSubjectName" class="font-weight-bold multisteps-form__input form-control" value="" type="text">
                                                        <input type="hidden" name="txtCourseID" value="${course_Item.courseID}"/>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                        <button type="submit" class="btn btn-primary">Add subject</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>                                    
                                </div>
                            </div>            
                        </div>    
                    </div>        
                </div>
        </section>

        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>
        <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js'></script>
        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script><script  src="./resources/js/addCourse.js"></script>
        <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script><script  src="./resources/js/date.js"></script>
    </body>
</html>
