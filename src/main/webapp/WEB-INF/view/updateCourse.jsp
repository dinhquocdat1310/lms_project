<%-- 
    Document   : updateCourse
    Created on : Jul 26, 2021, 10:17:41 PM
    Author     : Asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- BS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
      crossorigin="anonymous"
    />
    <!-- CDN Font awesome -->
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
    />
    <!-- Main -->
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css'>
    <link rel="stylesheet" href="./resources/css/style.css">
    <link rel="stylesheet" href="./resources/css/date.css">
    <title>Canvas</title>
  </head>
  <body>
    <section class="home__page">
      <div class="home__content">
        <div class="home__left">
          <img src="../IMG/canvas-min.jpg" alt="" srcset="" />
          <div class="select__Info">
            <a href="./profile.html">
              <i class="fas fa-user-circle"></i>
              <p class="select__link">Account</p>
            </a>
          </div>
          <div class="select__Info">
            <a href="pageCourseTeacher">
              <i class="fas fa-tachometer-alt"></i>
              <p class="select__link">Dashboard</p>
            </a>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#course"
            aria-controls="offcanvasScrolling"
          >
            <i class="fas fa-book"></i>
            <p>Courses</p>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <a href="./calendar.html">
              <i class="fas fa-calendar-alt"></i>
              <p>Calendar</p>
            </a>           
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <i class="fas fa-envelope-open-text"></i>
            <p>Inbox</p>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <i class="far fa-clock"></i>
            <p>History</p>
          </div>
          <div
            class="select__Info"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasScrolling"
            aria-controls="offcanvasScrolling"
          >
            <i class="fas fa-info-circle"></i>
            <p>Help</p>
          </div>
        </div>
        <div class="home__middle">
          <div class="middle__title">
            <h2>Add New Course</h2>          
          </div>        
          <div class="middle__detail">                     
            <div>
                <a href="finishAdd">
                    <button type="button" class="btn1 btn btn-outline-secondary">
                        Return
                    </button>
                </a>
            </div>          
          </div>
          <div class="container overflow-hidden">

            <div class="multisteps-form">
            
            <div class="row">
            <div class="col-12">
            <div class="multisteps-form__progress">
            <button class="multisteps-form__progress-btn js-active" type="button" title="Course Info">Course Info</button>
            <button class="multisteps-form__progress-btn" type="button" title="Comments">Review </button>
            </div>
            </div>
            </div>
            
            <div class="row">
            <div class="col-12">
            <form class="multisteps-form__form" action="updateItemCourse" method="POST">
            <div class="form-info">
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
                <h3 class="multisteps-form__title">Course Info</h3>
                <div class="multisteps-form__content">
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-12">
                        <input type="hidden" name="txtCourseID" value="${UC.courseID}"/>
                        <input name="txtCourseName" id="txtConfirmName" value="${UC.courseName}" class="multisteps-form__input form-control" type="text" placeholder="Course Name">
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-12">
                      <div class="date-picker">
                        <div class="input">
                            <div class="result">Date: <span id="txtDateStartAt" value="">${UC.startAt}</span>                            
                              <input type="hidden" id="dateCreateAt" name="txtDateCreateAt" value="" />
                            </div>
                          <button><i class="fa fa-calendar"></i></button>
                          <div class="result">Date: <span id="txtStartAt"></span></div>
                          <button class="calendar-btn"><i class="fa fa-calendar"></i></button>
                        </div>
                        <div class="calendar"></div>                        
                      </div>
                    </div>
                  </div>
                    <div class="form-check form-check-inline" style="margin-left: 10px; margin-top: 30px; margin-right: 20px">
                        <input class="form-check-input" type="radio" name="txtIsActive" id="Publish" value="true" <c:if test="${UC.isActive eq 'true'}"> checked="checked"</c:if> >
                        <label style="margin-left: 5px" class="form-check-label" for="Publish">Publish Course</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="txtIsActive" id="Unpublish" value="false" <c:if test="${UC.isActive eq 'false'}"> checked="checked"</c:if> >
                        <label style="margin-left: 5px" class="form-check-label" for="Unpublish">Unpublish Course</label>
                    </div>                            
                  <div class="form-row mt-4">
                    <div class="col-12 col-sm-12 mt-12 mt-sm-12">
                        <textarea name="txtTextArea" value="" placeholder="Course Description" class="multisteps-form__input form-control">${UC.description}</textarea>
                    </div>
                  </div>
                  <div class="button-row d-flex mt-4">
                    <button onclick="myFunction()" class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
            </div>               
            <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
            <h3 class="multisteps-form__title">Review</h3>
            <div class="multisteps-form__content">
            <div class="form-review">
                <div class="card mb-3">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Course Name </h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                        <h6 id="confirmName">                            
                        </h6>
                      </div>
                    </div>
                    <hr>
                    <div class="row">
                      <div class="col-sm-3">
                        <h6 class="mb-0">Course Start At  </h6>
                      </div>
                      <div class="col-sm-9 text-secondary">
                          <h6 id="startAt"></h6>
                      </div>
                  </div>
            </div>
            </div>
            </div>
            </div>             
            <div class="button-row d-flex mt-4">
              <div class="btn-left">
                <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
              </div>
              <div class="btn-right">
                <button class="btn btn-success ml-auto" type="submit" title="Send">Update Course</button>
              </div>    
            </div>     
            </div>
            </div>
            </div>
            </div>
        </form> 
    </section>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
      integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
      integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
      crossorigin="anonymous"
    ></script>
    <script src='https://code.jquery.com/jquery-3.2.1.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script><script  src="./resources/js/addCourse.js"></script>
    <script src='https://code.jquery.com/ui/1.12.1/jquery-ui.js'></script><script  src="./resources/js/date.js"></script>
  </body>
</html>

