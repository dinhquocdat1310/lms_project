/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function() {
  $( ".calendar" ).datepicker({
		dateFormat: 'dd/mm/yy',
		firstDay: 1
	});	
	$(document).on('click', '.date-picker .input', function(e){
		var $me = $(this),
				$parent = $me.parents('.date-picker');
		$parent.toggleClass('open');
	});		
	$(".calendar").on("change",function(){
		var $me = $(this),
				$selected = $me.val(),
				$parent = $me.parents('.date-picker');
		$parent.find('.result').children('span').html($selected);
		$("#dateCreateAt").attr('value', $selected);// work                
	});       
});

function myFunction() {    
    var courseNameInner = document.getElementById("txtConfirmName").value;
    document.getElementById("confirmName").innerHTML = courseNameInner;
    var startAtInner = document.getElementById("dateCreateAt").value;
    document.getElementById("startAt").innerHTML = startAtInner;    
}
