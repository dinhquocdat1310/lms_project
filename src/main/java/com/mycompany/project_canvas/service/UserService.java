/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.service;

import com.mycompany.project_canvas.dbo.TblUser;
import com.mycompany.project_canvas.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Asus
 */
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }       
    
    public List<TblUser> getTeacher(){
        return this.userRepository.getListTeacher();
    }
    
//    public List<TblUser> getListSearchStudent(String keyword){
//        return this.userRepository.searchStudent(keyword);
//    }
    
    public List<TblUser> getListStudentJoinCourseID(String className, Integer courseID){
        return this.userRepository.listStudentJoinID(className, courseID);
    }
    
    public List<TblUser> getListStudentJoinCourse(String userID){
        return this.userRepository.getListStudentJoinCourse(userID);
    }
    
    public TblUser checkUserLogin(String userID, String password){
        return this.userRepository.checkLogin(userID, password);
    }
}
