/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.service;

import com.mycompany.project_canvas.dbo.TblClass;
import com.mycompany.project_canvas.repository.ClassRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Asus
 */
@Service
public class ClassService {

    @Autowired
    private ClassRepository classRepository;

    public ClassService(ClassRepository classRepository) {
        this.classRepository = classRepository;
    }

    public void createNewClass(TblClass dtoClass) {
        this.classRepository.save(dtoClass);
    }

//    public Integer getClassIDBottom() {
//        return this.classRepository.getClassIDBottom();
//    }
    
    public List<Integer> getClassIDBottom() {
        return this.classRepository.getListClassIDBottom();
    }
    
    
}
