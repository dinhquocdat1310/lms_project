/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.service;

import com.mycompany.project_canvas.dbo.TblStudentCourse;
import com.mycompany.project_canvas.repository.StudentCourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Asus
 */
@Service
public class StudentCourseService {
    
    @Autowired
    private StudentCourseRepository studentCourseRepository;

    public StudentCourseService(StudentCourseRepository studentCourseRepository) {
        this.studentCourseRepository = studentCourseRepository;
    }
    
    public void addStudentJoinCourse(TblStudentCourse dtoStudentCourse){
        this.studentCourseRepository.save(dtoStudentCourse);
    }
    
    
}
