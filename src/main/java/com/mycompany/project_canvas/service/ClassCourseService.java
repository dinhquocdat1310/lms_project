/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.service;

import com.mycompany.project_canvas.dbo.TblClass;
import com.mycompany.project_canvas.dbo.TblClassCourse;
import com.mycompany.project_canvas.dbo.TblCourse;
import com.mycompany.project_canvas.repository.ClassCourseRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Asus
 */
@Service
public class ClassCourseService {

    @Autowired
    private ClassCourseRepository classCourseRepository;

    public ClassCourseService(ClassCourseRepository classCourseRepository) {
        this.classCourseRepository = classCourseRepository;
    }

    public void addClassCourse(TblClassCourse classCourse) {
        this.classCourseRepository.save(classCourse);
    }

    public Integer getClassCourseID(TblClass classID, TblCourse courseID) {
        return this.classCourseRepository.getListSubjectByCourseID(classID, courseID);
    }
    
    public List<Integer> getClassIDByCourseID(TblCourse courseID) {
        return this.classCourseRepository.getListClassIDByCourseID(courseID);
    }
}
