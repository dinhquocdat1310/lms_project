/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.service;

import com.mycompany.project_canvas.dbo.TblCourse;
import com.mycompany.project_canvas.dbo.TblSubject;
import com.mycompany.project_canvas.repository.SubjectRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Asus
 */
@Service
public class SubjectService {
    
    @Autowired
    private SubjectRepository subjectRepository;

    public SubjectService(SubjectRepository subjectRepository) {
        this.subjectRepository = subjectRepository;
    }
    
    public void createNewSubject(TblSubject dto_Subject){
        this.subjectRepository.save(dto_Subject);
    }               

    public List<TblSubject> getListSubjectByIDCourse(TblCourse courseID){
        return subjectRepository.getListSubjectByCourseID(courseID);
    }
    
    public void deleteSubjectById(Integer subjectID){
        this.subjectRepository.deleteById(subjectID);
    }
    
    public List<TblSubject> findAllSubject(){
        return this.subjectRepository.findAll();
    }
    
    public TblSubject findSubjectById(Integer subjectID){        
        Optional<TblSubject> result = subjectRepository.findById(subjectID);
        return result.get();        
    }
    
    public void updateSubject(Integer subjectID, String txtSubjectName, boolean isActive){
        this.subjectRepository.updateSubjectByID(txtSubjectName, isActive, subjectID);
    }
}
