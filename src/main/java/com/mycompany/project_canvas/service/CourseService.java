/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.service;

import com.mycompany.project_canvas.dbo.TblCourse;
import com.mycompany.project_canvas.repository.CourseRepository;
import java.sql.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Asus
 */
@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public void createNewCourse(TblCourse dto) {
        this.courseRepository.save(dto);
    }

    public List<TblCourse> getListCourse() {
        return this.courseRepository.findAll();
    }

    public TblCourse getCourse(Integer id) {
        Optional<TblCourse> result = courseRepository.findById(id);
        return result.get();
    }

    public List<TblCourse> getCourseActive() {
        return courseRepository.getListCoursePublish();
    }

    public List<TblCourse> getCourseInActive() {
        return courseRepository.getListCourseUnpublish();
    }

    public void updateCourseItemByID(Integer courseID, String courseName, String description, Date startAt, boolean isActive) {
        this.courseRepository.updateCourseByID(courseName, description, startAt, isActive, courseID);
    }

    public List<Integer> getCourseID() {
        return this.courseRepository.getListCourseID();
    }

    public List<String> getListClassNameByCourseID (TblCourse courseID){
       return this.courseRepository.getListClassName(courseID);
    }
}
