/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblCourse", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblCourse.findAll", query = "SELECT t FROM TblCourse t")
    , @NamedQuery(name = "TblCourse.findByCourseID", query = "SELECT t FROM TblCourse t WHERE t.courseID = :courseID")
    , @NamedQuery(name = "TblCourse.findByCourseName", query = "SELECT t FROM TblCourse t WHERE t.courseName = :courseName")
    , @NamedQuery(name = "TblCourse.findByDescription", query = "SELECT t FROM TblCourse t WHERE t.description = :description")
    , @NamedQuery(name = "TblCourse.findByStartAt", query = "SELECT t FROM TblCourse t WHERE t.startAt = :startAt")
    , @NamedQuery(name = "TblCourse.findByIsActive", query = "SELECT t FROM TblCourse t WHERE t.isActive = :isActive")
    , @NamedQuery(name = "TblCourse.findByCreateDate", query = "SELECT t FROM TblCourse t WHERE t.createDate = :createDate")})
public class TblCourse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "courseID", nullable = false)
    private Integer courseID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "courseName", nullable = false, length = 50)
    private String courseName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description", nullable = false, length = 50)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "startAt", nullable = false, length = 10)
    private Date startAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "createDate", nullable = false, length = 10)
    private Date createDate;
    @JoinColumn(name = "createUserID", referencedColumnName = "userID", nullable = false)
    @ManyToOne(optional = false)
    private TblUser createUserID;
    @OneToMany(mappedBy = "courseID")
    private Collection<TblClassCourse> tblClassCourseCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "courseID")
    private Collection<TblSubject> tblSubjectCollection;

    public TblCourse() {
    }

    public TblCourse(Integer courseID) {
        this.courseID = courseID;
    }

    public TblCourse(String courseName, String description, Date startAt, boolean isActive, Date createDate, TblUser createUserID) {
        this.courseName = courseName;
        this.description = description;
        this.startAt = startAt;
        this.isActive = isActive;
        this.createDate = createDate;
        this.createUserID = createUserID;
    }        
    
    public TblCourse(Integer courseID, String courseName, String description, Date startAt, boolean isActive, Date createDate) {
        this.courseID = courseID;
        this.courseName = courseName;
        this.description = description;
        this.startAt = startAt;
        this.isActive = isActive;
        this.createDate = createDate;
    }        

    public Integer getCourseID() {
        return courseID;
    }

    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public TblUser getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(TblUser createUserID) {
        this.createUserID = createUserID;
    }

    @XmlTransient
    public Collection<TblClassCourse> getTblClassCourseCollection() {
        return tblClassCourseCollection;
    }

    public void setTblClassCourseCollection(Collection<TblClassCourse> tblClassCourseCollection) {
        this.tblClassCourseCollection = tblClassCourseCollection;
    }

    @XmlTransient
    public Collection<TblSubject> getTblSubjectCollection() {
        return tblSubjectCollection;
    }

    public void setTblSubjectCollection(Collection<TblSubject> tblSubjectCollection) {
        this.tblSubjectCollection = tblSubjectCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (courseID != null ? courseID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblCourse)) {
            return false;
        }
        TblCourse other = (TblCourse) object;
        if ((this.courseID == null && other.courseID != null) || (this.courseID != null && !this.courseID.equals(other.courseID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblCourse[ courseID=" + courseID + " ]";
    }
    
}
