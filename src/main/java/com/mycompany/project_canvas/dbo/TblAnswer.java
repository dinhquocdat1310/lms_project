/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblAnswer", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblAnswer.findAll", query = "SELECT t FROM TblAnswer t")
    , @NamedQuery(name = "TblAnswer.findByAnswerID", query = "SELECT t FROM TblAnswer t WHERE t.answerID = :answerID")
    , @NamedQuery(name = "TblAnswer.findByAnswerContent", query = "SELECT t FROM TblAnswer t WHERE t.answerContent = :answerContent")
    , @NamedQuery(name = "TblAnswer.findByIsRight", query = "SELECT t FROM TblAnswer t WHERE t.isRight = :isRight")})
public class TblAnswer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "answerID", nullable = false)
    private Integer answerID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "answerContent", nullable = false, length = 500)
    private String answerContent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isRight", nullable = false)
    private boolean isRight;
    @JoinColumn(name = "questionID", referencedColumnName = "questionID", nullable = false)
    @ManyToOne(optional = false)
    private TblQuestion questionID;

    public TblAnswer() {
    }

    public TblAnswer(Integer answerID) {
        this.answerID = answerID;
    }

    public TblAnswer(Integer answerID, String answerContent, boolean isRight) {
        this.answerID = answerID;
        this.answerContent = answerContent;
        this.isRight = isRight;
    }

    public Integer getAnswerID() {
        return answerID;
    }

    public void setAnswerID(Integer answerID) {
        this.answerID = answerID;
    }

    public String getAnswerContent() {
        return answerContent;
    }

    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent;
    }

    public boolean getIsRight() {
        return isRight;
    }

    public void setIsRight(boolean isRight) {
        this.isRight = isRight;
    }

    public TblQuestion getQuestionID() {
        return questionID;
    }

    public void setQuestionID(TblQuestion questionID) {
        this.questionID = questionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (answerID != null ? answerID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblAnswer)) {
            return false;
        }
        TblAnswer other = (TblAnswer) object;
        if ((this.answerID == null && other.answerID != null) || (this.answerID != null && !this.answerID.equals(other.answerID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblAnswer[ answerID=" + answerID + " ]";
    }
    
}
