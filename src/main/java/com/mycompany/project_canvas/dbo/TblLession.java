/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblLession", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblLession.findAll", query = "SELECT t FROM TblLession t")
    , @NamedQuery(name = "TblLession.findByLessionID", query = "SELECT t FROM TblLession t WHERE t.lessionID = :lessionID")})
public class TblLession implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "lessionID", nullable = false)
    private Integer lessionID;
    @JoinColumn(name = "subjectID", referencedColumnName = "subjectID", nullable = false)
    @ManyToOne(optional = false)
    private TblSubject subjectID;

    public TblLession() {
    }

    public TblLession(Integer lessionID) {
        this.lessionID = lessionID;
    }

    public Integer getLessionID() {
        return lessionID;
    }

    public void setLessionID(Integer lessionID) {
        this.lessionID = lessionID;
    }

    public TblSubject getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(TblSubject subjectID) {
        this.subjectID = subjectID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lessionID != null ? lessionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblLession)) {
            return false;
        }
        TblLession other = (TblLession) object;
        if ((this.lessionID == null && other.lessionID != null) || (this.lessionID != null && !this.lessionID.equals(other.lessionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblLession[ lessionID=" + lessionID + " ]";
    }

}
