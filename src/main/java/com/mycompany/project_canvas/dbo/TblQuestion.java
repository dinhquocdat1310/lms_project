/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblQuestion", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblQuestion.findAll", query = "SELECT t FROM TblQuestion t")
    , @NamedQuery(name = "TblQuestion.findByQuestionID", query = "SELECT t FROM TblQuestion t WHERE t.questionID = :questionID")
    , @NamedQuery(name = "TblQuestion.findByCreateDate", query = "SELECT t FROM TblQuestion t WHERE t.createDate = :createDate")
    , @NamedQuery(name = "TblQuestion.findByCreateUserID", query = "SELECT t FROM TblQuestion t WHERE t.createUserID = :createUserID")
    , @NamedQuery(name = "TblQuestion.findByQuestionContent", query = "SELECT t FROM TblQuestion t WHERE t.questionContent = :questionContent")
    , @NamedQuery(name = "TblQuestion.findByIsActive", query = "SELECT t FROM TblQuestion t WHERE t.isActive = :isActive")})
public class TblQuestion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "questionID", nullable = false)
    private Integer questionID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "createDate", nullable = false, length = 10)
    private String createDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "createUserID", nullable = false, length = 50)
    private String createUserID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "questionContent", nullable = false, length = 500)
    private String questionContent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "questionID")
    private Collection<TblAnswer> tblAnswerCollection;
    @JoinColumn(name = "subjectID", referencedColumnName = "subjectID", nullable = false)
    @ManyToOne(optional = false)
    private TblSubject subjectID;

    public TblQuestion() {
    }

    public TblQuestion(Integer questionID) {
        this.questionID = questionID;
    }

    public TblQuestion(Integer questionID, String createDate, String createUserID, String questionContent, boolean isActive) {
        this.questionID = questionID;
        this.createDate = createDate;
        this.createUserID = createUserID;
        this.questionContent = questionContent;
        this.isActive = isActive;
    }

    public Integer getQuestionID() {
        return questionID;
    }

    public void setQuestionID(Integer questionID) {
        this.questionID = questionID;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(String createUserID) {
        this.createUserID = createUserID;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    @XmlTransient
    public Collection<TblAnswer> getTblAnswerCollection() {
        return tblAnswerCollection;
    }

    public void setTblAnswerCollection(Collection<TblAnswer> tblAnswerCollection) {
        this.tblAnswerCollection = tblAnswerCollection;
    }

    public TblSubject getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(TblSubject subjectID) {
        this.subjectID = subjectID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (questionID != null ? questionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblQuestion)) {
            return false;
        }
        TblQuestion other = (TblQuestion) object;
        if ((this.questionID == null && other.questionID != null) || (this.questionID != null && !this.questionID.equals(other.questionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblQuestion[ questionID=" + questionID + " ]";
    }
    
}
