/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblQuiz", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblQuiz.findAll", query = "SELECT t FROM TblQuiz t")
    , @NamedQuery(name = "TblQuiz.findByQuizID", query = "SELECT t FROM TblQuiz t WHERE t.quizID = :quizID")
    , @NamedQuery(name = "TblQuiz.findByQuizName", query = "SELECT t FROM TblQuiz t WHERE t.quizName = :quizName")
    , @NamedQuery(name = "TblQuiz.findByQuizNum", query = "SELECT t FROM TblQuiz t WHERE t.quizNum = :quizNum")
    , @NamedQuery(name = "TblQuiz.findByQuizTime", query = "SELECT t FROM TblQuiz t WHERE t.quizTime = :quizTime")
    , @NamedQuery(name = "TblQuiz.findByStartAt", query = "SELECT t FROM TblQuiz t WHERE t.startAt = :startAt")
    , @NamedQuery(name = "TblQuiz.findByEndAt", query = "SELECT t FROM TblQuiz t WHERE t.endAt = :endAt")
    , @NamedQuery(name = "TblQuiz.findByIsActive", query = "SELECT t FROM TblQuiz t WHERE t.isActive = :isActive")})
public class TblQuiz implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "quizID", nullable = false)
    private Integer quizID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "quizName", nullable = false, length = 50)
    private String quizName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quizNum", nullable = false)
    private int quizNum;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "quizTime", nullable = false, length = 50)
    private String quizTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startAt", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "endAt", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endAt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    @JoinColumn(name = "subjectID", referencedColumnName = "subjectID", nullable = false)
    @ManyToOne(optional = false)
    private TblSubject subjectID;

    public TblQuiz() {
    }

    public TblQuiz(Integer quizID) {
        this.quizID = quizID;
    }

    public TblQuiz(Integer quizID, String quizName, int quizNum, String quizTime, Date startAt, Date endAt, boolean isActive) {
        this.quizID = quizID;
        this.quizName = quizName;
        this.quizNum = quizNum;
        this.quizTime = quizTime;
        this.startAt = startAt;
        this.endAt = endAt;
        this.isActive = isActive;
    }

    public Integer getQuizID() {
        return quizID;
    }

    public void setQuizID(Integer quizID) {
        this.quizID = quizID;
    }

    public String getQuizName() {
        return quizName;
    }

    public void setQuizName(String quizName) {
        this.quizName = quizName;
    }

    public int getQuizNum() {
        return quizNum;
    }

    public void setQuizNum(int quizNum) {
        this.quizNum = quizNum;
    }

    public String getQuizTime() {
        return quizTime;
    }

    public void setQuizTime(String quizTime) {
        this.quizTime = quizTime;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public TblSubject getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(TblSubject subjectID) {
        this.subjectID = subjectID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (quizID != null ? quizID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblQuiz)) {
            return false;
        }
        TblQuiz other = (TblQuiz) object;
        if ((this.quizID == null && other.quizID != null) || (this.quizID != null && !this.quizID.equals(other.quizID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblQuiz[ quizID=" + quizID + " ]";
    }
    
}
