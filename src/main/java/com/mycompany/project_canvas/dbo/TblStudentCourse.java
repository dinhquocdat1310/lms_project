/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblStudentCourse", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblStudentCourse.findAll", query = "SELECT t FROM TblStudentCourse t")
    , @NamedQuery(name = "TblStudentCourse.findByStudentJoinID", query = "SELECT t FROM TblStudentCourse t WHERE t.studentJoinID = :studentJoinID")
    , @NamedQuery(name = "TblStudentCourse.findByCourseMark", query = "SELECT t FROM TblStudentCourse t WHERE t.courseMark = :courseMark")
    , @NamedQuery(name = "TblStudentCourse.findByJoinDate", query = "SELECT t FROM TblStudentCourse t WHERE t.joinDate = :joinDate")})
public class TblStudentCourse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "studentJoinID", nullable = false, length = 50)
    private String studentJoinID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "courseMark", nullable = false)
    private double courseMark;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "joinDate", nullable = false, length = 10)
    private Date joinDate;
    @JoinColumn(name = "classCourseID", referencedColumnName = "classCourseID")
    @ManyToOne
    private TblClassCourse classCourseID;
//    @JoinColumn(name = "fbID", referencedColumnName = "fbID")
//    @ManyToOne
//    private TblFeedback fbID;

    public TblStudentCourse() {
    }

    public TblStudentCourse(String studentJoinID) {
        this.studentJoinID = studentJoinID;
    }

    public TblStudentCourse(String studentJoinID, double courseMark, Date joinDate) {
        this.studentJoinID = studentJoinID;
        this.courseMark = courseMark;
        this.joinDate = joinDate;
    }

    public TblStudentCourse(String studentJoinID, double courseMark, Date joinDate, TblClassCourse classCourseID) {
        this.studentJoinID = studentJoinID;
        this.courseMark = courseMark;
        this.joinDate = joinDate;
        this.classCourseID = classCourseID;
    }       
    
    public String getStudentJoinID() {
        return studentJoinID;
    }

    public void setStudentJoinID(String studentJoinID) {
        this.studentJoinID = studentJoinID;
    }

    public double getCourseMark() {
        return courseMark;
    }

    public void setCourseMark(double courseMark) {
        this.courseMark = courseMark;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public TblClassCourse getClassCourseID() {
        return classCourseID;
    }

    public void setClassCourseID(TblClassCourse classCourseID) {
        this.classCourseID = classCourseID;
    }

//    public TblFeedback getFbID() {
//        return fbID;
//    }
//
//    public void setFbID(TblFeedback fbID) {
//        this.fbID = fbID;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (studentJoinID != null ? studentJoinID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblStudentCourse)) {
            return false;
        }
        TblStudentCourse other = (TblStudentCourse) object;
        if ((this.studentJoinID == null && other.studentJoinID != null) || (this.studentJoinID != null && !this.studentJoinID.equals(other.studentJoinID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblStudentCourse[ studentJoinID=" + studentJoinID + " ]";
    }
    
}
