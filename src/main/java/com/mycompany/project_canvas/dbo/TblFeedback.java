/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblFeedback", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblFeedback.findAll", query = "SELECT t FROM TblFeedback t")
    , @NamedQuery(name = "TblFeedback.findByFbID", query = "SELECT t FROM TblFeedback t WHERE t.fbID = :fbID")
    , @NamedQuery(name = "TblFeedback.findByTitle", query = "SELECT t FROM TblFeedback t WHERE t.title = :title")
    , @NamedQuery(name = "TblFeedback.findByMessage", query = "SELECT t FROM TblFeedback t WHERE t.message = :message")})
public class TblFeedback implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fbID", nullable = false)
    private Integer fbID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "title", nullable = false, length = 50)
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "message", nullable = false, length = 500)
    private String message;    
    @JoinColumn(name = "classCourseID", referencedColumnName = "classCourseID", nullable = false)
    @ManyToOne(optional = false)
    private TblClassCourse classCourseID;

    public TblFeedback() {
    }

    public TblFeedback(Integer fbID) {
        this.fbID = fbID;
    }

    public TblFeedback(Integer fbID, String title, String message) {
        this.fbID = fbID;
        this.title = title;
        this.message = message;
    }

    public Integer getFbID() {
        return fbID;
    }

    public void setFbID(Integer fbID) {
        this.fbID = fbID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }    

    public TblClassCourse getClassCourseID() {
        return classCourseID;
    }

    public void setClassCourseID(TblClassCourse classCourseID) {
        this.classCourseID = classCourseID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fbID != null ? fbID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblFeedback)) {
            return false;
        }
        TblFeedback other = (TblFeedback) object;
        if ((this.fbID == null && other.fbID != null) || (this.fbID != null && !this.fbID.equals(other.fbID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblFeedback[ fbID=" + fbID + " ]";
    }
    
}
