/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblEvent", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblEvent.findAll", query = "SELECT t FROM TblEvent t")
    , @NamedQuery(name = "TblEvent.findByEventID", query = "SELECT t FROM TblEvent t WHERE t.eventID = :eventID")
    , @NamedQuery(name = "TblEvent.findByEventName", query = "SELECT t FROM TblEvent t WHERE t.eventName = :eventName")
    , @NamedQuery(name = "TblEvent.findByDescription", query = "SELECT t FROM TblEvent t WHERE t.description = :description")
    , @NamedQuery(name = "TblEvent.findByCreateDate", query = "SELECT t FROM TblEvent t WHERE t.createDate = :createDate")
    , @NamedQuery(name = "TblEvent.findByEventURL", query = "SELECT t FROM TblEvent t WHERE t.eventURL = :eventURL")
    , @NamedQuery(name = "TblEvent.findByIsActive", query = "SELECT t FROM TblEvent t WHERE t.isActive = :isActive")})
public class TblEvent implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "eventID", nullable = false)
    private Integer eventID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "eventName", nullable = false, length = 50)
    private String eventName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "description", nullable = false, length = 50)
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "createDate", nullable = false, length = 10)
    private String createDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "eventURL", nullable = false, length = 250)
    private String eventURL;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    @JoinColumn(name = "classCourseID", referencedColumnName = "classCourseID", nullable = false)
    @ManyToOne(optional = false)
    private TblClassCourse classCourseID;

    public TblEvent() {
    }

    public TblEvent(Integer eventID) {
        this.eventID = eventID;
    }

    public TblEvent(Integer eventID, String eventName, String description, String createDate, String eventURL, boolean isActive) {
        this.eventID = eventID;
        this.eventName = eventName;
        this.description = description;
        this.createDate = createDate;
        this.eventURL = eventURL;
        this.isActive = isActive;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getEventURL() {
        return eventURL;
    }

    public void setEventURL(String eventURL) {
        this.eventURL = eventURL;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public TblClassCourse getClassCourseID() {
        return classCourseID;
    }

    public void setClassCourseID(TblClassCourse classCourseID) {
        this.classCourseID = classCourseID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventID != null ? eventID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblEvent)) {
            return false;
        }
        TblEvent other = (TblEvent) object;
        if ((this.eventID == null && other.eventID != null) || (this.eventID != null && !this.eventID.equals(other.eventID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblEvent[ eventID=" + eventID + " ]";
    }
    
}
