/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblSubject", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblSubject.findAll", query = "SELECT t FROM TblSubject t")
    , @NamedQuery(name = "TblSubject.findBySubjectID", query = "SELECT t FROM TblSubject t WHERE t.subjectID = :subjectID")
    , @NamedQuery(name = "TblSubject.findBySubjectName", query = "SELECT t FROM TblSubject t WHERE t.subjectName = :subjectName")})
public class TblSubject implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "subjectID", nullable = false)
    private Integer subjectID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "subjectName", nullable = false, length = 50)
    private String subjectName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isActive")
    private boolean isActive;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subjectID")
    private Collection<TblLession> tblLessionCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subjectID")
    private Collection<TblQuiz> tblQuizCollection;
    @JoinColumn(name = "courseID", referencedColumnName = "courseID", nullable = false)
    @ManyToOne(optional = false)
    private TblCourse courseID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "subjectID")
    private Collection<TblQuestion> tblQuestionCollection;

    public TblSubject() {
    }

    public TblSubject(Integer subjectID) {
        this.subjectID = subjectID;
    }

    public TblSubject(Integer subjectID, String subjectName) {
        this.subjectID = subjectID;
        this.subjectName = subjectName;
    }

    public TblSubject(String subjectName, boolean isActive, TblCourse courseID) {
        this.subjectName = subjectName;
        this.isActive = isActive;
        this.courseID = courseID;
    }   

    public Integer getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(Integer subjectID) {
        this.subjectID = subjectID;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    @XmlTransient
    public Collection<TblLession> getTblLessionCollection() {
        return tblLessionCollection;
    }

    public void setTblLessionCollection(Collection<TblLession> tblLessionCollection) {
        this.tblLessionCollection = tblLessionCollection;
    }

    @XmlTransient
    public Collection<TblQuiz> getTblQuizCollection() {
        return tblQuizCollection;
    }

    public void setTblQuizCollection(Collection<TblQuiz> tblQuizCollection) {
        this.tblQuizCollection = tblQuizCollection;
    }

    public TblCourse getCourseID() {
        return courseID;
    }

    public void setCourseID(TblCourse courseID) {
        this.courseID = courseID;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }        
    
    @XmlTransient
    public Collection<TblQuestion> getTblQuestionCollection() {
        return tblQuestionCollection;
    }

    public void setTblQuestionCollection(Collection<TblQuestion> tblQuestionCollection) {
        this.tblQuestionCollection = tblQuestionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subjectID != null ? subjectID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblSubject)) {
            return false;
        }
        TblSubject other = (TblSubject) object;
        if ((this.subjectID == null && other.subjectID != null) || (this.subjectID != null && !this.subjectID.equals(other.subjectID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblSubject[ subjectID=" + subjectID + " ]";
    }
    
}
