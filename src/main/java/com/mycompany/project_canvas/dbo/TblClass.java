/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblClass", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblClass.findAll", query = "SELECT t FROM TblClass t")
    , @NamedQuery(name = "TblClass.findByClassID", query = "SELECT t FROM TblClass t WHERE t.classID = :classID")
    , @NamedQuery(name = "TblClass.findByCreateDate", query = "SELECT t FROM TblClass t WHERE t.createDate = :createDate")
    , @NamedQuery(name = "TblClass.findByClassName", query = "SELECT t FROM TblClass t WHERE t.className = :className")
    , @NamedQuery(name = "TblClass.findByIsActive", query = "SELECT t FROM TblClass t WHERE t.isActive = :isActive")})
public class TblClass implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "classID", nullable = false)
    private Integer classID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "createDate", nullable = false, length = 10)
    private Date createDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberOfStudent")
    private Integer numberOfStudent;   
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "className", nullable = false, length = 50)
    private String className;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    @OneToMany(mappedBy = "classID")
    private Collection<TblClassCourse> tblClassCourseCollection;

    public TblClass() {
    }

    public TblClass(Integer classID) {
        this.classID = classID;
    }

    public TblClass(Integer classID, Date createDate, String className, boolean isActive) {
        this.classID = classID;
        this.createDate = createDate;
        this.className = className;
        this.isActive = isActive;
    }

    public TblClass(Date createDate, Integer numberOfStudent, String className, boolean isActive) {
        this.createDate = createDate;
        this.numberOfStudent = numberOfStudent;
        this.className = className;
        this.isActive = isActive;
    }      
    
    public Integer getClassID() {
        return classID;
    }

    public void setClassID(Integer classID) {
        this.classID = classID;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(Integer numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }   
    
    @XmlTransient
    public Collection<TblClassCourse> getTblClassCourseCollection() {
        return tblClassCourseCollection;
    }

    public void setTblClassCourseCollection(Collection<TblClassCourse> tblClassCourseCollection) {
        this.tblClassCourseCollection = tblClassCourseCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classID != null ? classID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblClass)) {
            return false;
        }
        TblClass other = (TblClass) object;
        if ((this.classID == null && other.classID != null) || (this.classID != null && !this.classID.equals(other.classID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblClass[ classID=" + classID + " ]";
    }
    
}
