/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.dbo;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "tblClassCourse", catalog = "CanvasDB", schema = "dbo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblClassCourse.findAll", query = "SELECT t FROM TblClassCourse t")
    , @NamedQuery(name = "TblClassCourse.findByClassCourseID", query = "SELECT t FROM TblClassCourse t WHERE t.classCourseID = :classCourseID")})
public class TblClassCourse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "classCourseID", nullable = false)
    private Integer classCourseID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classCourseID")
    private Collection<TblEvent> tblEventCollection;
    @OneToMany(mappedBy = "classCourseID")
    private Collection<TblStudentCourse> tblStudentCourseCollection;
    @JoinColumn(name = "classID", referencedColumnName = "classID")
    @ManyToOne
    private TblClass classID;
    @JoinColumn(name = "courseID", referencedColumnName = "courseID")
    @ManyToOne
    private TblCourse courseID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classCourseID")
    private Collection<TblFeedback> tblFeedbackCollection;

    public TblClassCourse() {
    }

    public TblClassCourse(Integer classCourseID) {
        this.classCourseID = classCourseID;
    }

    public TblClassCourse(TblClass classID, TblCourse courseID) {
        this.classID = classID;
        this.courseID = courseID;
    }    
    
    public Integer getClassCourseID() {
        return classCourseID;
    }

    public void setClassCourseID(Integer classCourseID) {
        this.classCourseID = classCourseID;
    }

    @XmlTransient
    public Collection<TblEvent> getTblEventCollection() {
        return tblEventCollection;
    }

    public void setTblEventCollection(Collection<TblEvent> tblEventCollection) {
        this.tblEventCollection = tblEventCollection;
    }

    @XmlTransient
    public Collection<TblStudentCourse> getTblStudentCourseCollection() {
        return tblStudentCourseCollection;
    }

    public void setTblStudentCourseCollection(Collection<TblStudentCourse> tblStudentCourseCollection) {
        this.tblStudentCourseCollection = tblStudentCourseCollection;
    }

    public TblClass getClassID() {
        return classID;
    }

    public void setClassID(TblClass classID) {
        this.classID = classID;
    }

    public TblCourse getCourseID() {
        return courseID;
    }

    public void setCourseID(TblCourse courseID) {
        this.courseID = courseID;
    }

    @XmlTransient
    public Collection<TblFeedback> getTblFeedbackCollection() {
        return tblFeedbackCollection;
    }

    public void setTblFeedbackCollection(Collection<TblFeedback> tblFeedbackCollection) {
        this.tblFeedbackCollection = tblFeedbackCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (classCourseID != null ? classCourseID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblClassCourse)) {
            return false;
        }
        TblClassCourse other = (TblClassCourse) object;
        if ((this.classCourseID == null && other.classCourseID != null) || (this.classCourseID != null && !this.classCourseID.equals(other.classCourseID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.project_canvas.dbo.TblClassCourse[ classCourseID=" + classCourseID + " ]";
    }
    
}
