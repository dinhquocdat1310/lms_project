/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.controller;

import com.mycompany.project_canvas.dbo.TblClass;
import com.mycompany.project_canvas.dbo.TblClassCourse;
import com.mycompany.project_canvas.dbo.TblCourse;
//import com.mycompany.project_canvas.dbo.TblRole;
import com.mycompany.project_canvas.dbo.TblStudentCourse;
import com.mycompany.project_canvas.dbo.TblSubject;
import com.mycompany.project_canvas.dbo.TblUser;
import com.mycompany.project_canvas.service.ClassCourseService;
import com.mycompany.project_canvas.service.ClassService;
import com.mycompany.project_canvas.service.CourseService;
import com.mycompany.project_canvas.service.StudentCourseService;
import com.mycompany.project_canvas.service.SubjectService;
import com.mycompany.project_canvas.service.UserService;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Asus
 */
@Controller
public class MainController {

    @Autowired
    private UserService userService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private SubjectService subjectService;
    @Autowired
    private ClassService classService;
    @Autowired
    private ClassCourseService classCourseService;
    @Autowired
    private StudentCourseService studentCourseService;

//    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getLoadPublishAndUnpublish() {
        ModelAndView mav = new ModelAndView("pageCourseTeacher");
        List<TblCourse> listPublish = courseService.getCourseActive();
        List<TblCourse> listUnpublish = courseService.getCourseInActive();
        for (TblCourse tblCourse : listPublish) {
            System.out.println(tblCourse.getCourseName() + " ---(pageCourseTeacher)");
        }
        for (TblCourse tblCourse : listUnpublish) {
            System.out.println(tblCourse.getCourseName() + " --- Ra roi (pageCourseTeacher)");
        }
        mav.addObject("listCourseActive", listPublish);
        mav.addObject("listCourseInActive", listUnpublish);
        return mav;
    }

    @RequestMapping(value = "pageCourseTeacher", method = RequestMethod.GET)
    public ModelAndView getLoadDashBoard() {
        return getLoadPublishAndUnpublish();
    }

    @RequestMapping("/listCourse")
    public ModelAndView getLoadAllCourse() {
        ModelAndView mav = new ModelAndView("listCourse");
        List<TblCourse> list = courseService.getListCourse();
        for (TblCourse tblCourse : list) {
            System.out.println(tblCourse.getCourseName() + " --- Ra roi (pageCourseTeacher)");
        }
        mav.addObject("listCourse", list);
        List<TblCourse> listPublish = courseService.getCourseActive();
        List<TblCourse> listUnpublish = courseService.getCourseInActive();
        for (TblCourse tblCourse : listPublish) {
            System.out.println(tblCourse.getCourseName() + " ---(pageCourseTeacher)");
        }
        for (TblCourse tblCourse : listUnpublish) {
            System.out.println(tblCourse.getCourseName() + " --- Ra roi (pageCourseTeacher)");
        }
        mav.addObject("listCourseActive", listPublish);
        mav.addObject("listCourseInActive", listUnpublish);
        return mav;
    }

    @RequestMapping(value = "/createCourse")
    public ModelAndView createCourse() {
        ModelAndView mav = new ModelAndView("createCourse");
        return mav;
    }

    @RequestMapping(value = "/addNewCourse")
    public String addNewCourse(HttpSession httpSession, @RequestParam String txtCourseName, @RequestParam String txtTextArea, @RequestParam String txtDateCreateAt) throws ParseException {
        java.util.Date date = new java.util.Date();
        Date createDate = new Date(date.getTime());
        java.sql.Date dateParse = new java.sql.Date(((java.util.Date) new SimpleDateFormat("dd/MM/yyyy").parse(txtDateCreateAt)).getTime());
        TblUser createUser = new TblUser("SE123456");
        TblCourse new_DTO = new TblCourse(txtCourseName, txtTextArea, dateParse, false, createDate, createUser);
        courseService.createNewCourse(new_DTO);
        return "redirect:/";
    }

    //Create Subject
    @RequestMapping(value = "/subjectCreate")
    public ModelAndView createSubject(@RequestParam Integer courseID) {
        return getObjectModal(courseID);
    }

    @RequestMapping(value = "/addNewSubject")
    public ModelAndView addNewSubject(@RequestParam String txtSubjectName, @RequestParam Integer txtCourseID) {
        TblCourse courseIDParse = new TblCourse(txtCourseID);
        System.out.println(txtSubjectName + " subject Name");
        System.out.println(txtCourseID + " courseID");
        TblSubject newDTO_Subject = new TblSubject(txtSubjectName, false, courseIDParse);
        subjectService.createNewSubject(newDTO_Subject);
        return getObjectModal(txtCourseID);
    }

    public List<TblSubject> loadAllSubject(Integer courseID) {
        TblCourse tbl_CourseID = new TblCourse(courseID);
        List<TblSubject> list_Subject = subjectService.getListSubjectByIDCourse(tbl_CourseID);
        return list_Subject;
    }

    public ModelAndView getObjectModal(Integer courseID) {
        ModelAndView mav = new ModelAndView("subjectCreate");
        TblCourse courseItem = courseService.getCourse(courseID);
        mav.addObject("course_Item", courseItem);
        mav.addObject("listSubject", loadAllSubject(courseID));
        return mav;
    }

    @RequestMapping("/updateCourse")
    public ModelAndView updateCoursePage(@RequestParam Integer courseID) {
        ModelAndView mav = new ModelAndView("updateCourse");
        TblCourse getCourse = courseService.getCourse(courseID);
        mav.addObject("UC", getCourse);
        return mav;
    }

    @RequestMapping(value = "/updateItemCourse")
    public String updateCourse(@RequestParam String txtCourseID, @RequestParam String txtCourseName, @RequestParam String txtTextArea, @RequestParam String txtDateCreateAt, @RequestParam String txtIsActive) throws ParseException {
        java.sql.Date dateParse = new java.sql.Date(((java.util.Date) new SimpleDateFormat("dd/MM/yyyy").parse(txtDateCreateAt)).getTime());
        Integer parseCourseID = Integer.parseInt(txtCourseID);
        boolean checked = false;
        if (txtIsActive.equalsIgnoreCase("true")) {
            checked = true;
        }
        courseService.updateCourseItemByID(parseCourseID, txtCourseName, txtTextArea, dateParse, checked);
        return "redirect:/";
    }

    @RequestMapping("/finishAdd")
    public ModelAndView finishAddSub() {
        return getLoadPublishAndUnpublish();
    }

    @RequestMapping("/actionEditSubject")
    public ModelAndView loadActionSubject(@RequestParam String txtSubjectID, @RequestParam String txtCourseID) {
        ModelAndView mav = new ModelAndView("actionSubject");
        Integer parseSubjectID = Integer.parseInt(txtSubjectID);
        Integer parseCourseID = Integer.parseInt(txtCourseID);
        TblSubject getSubject = subjectService.findSubjectById(parseSubjectID);
        System.out.println(parseCourseID + " action Edit Subject courseID");
        mav.addObject("ITSJ", getSubject);
        mav.addObject("COURSEID", parseCourseID);
        return mav;
    }

    @RequestMapping("/confirmDeleteSubject")
    public String deleteSubjectByID(@RequestParam String txt_SubjectID) {
        Integer parseSubjectID = Integer.parseInt(txt_SubjectID);
        System.out.println(txt_SubjectID + " subject id");
        subjectService.deleteSubjectById(parseSubjectID);
        return "redirect:/";
    }

    @RequestMapping("/updateSubject")
    public String updateSubject(@RequestParam String txtCourseID, @RequestParam String txtSubjectIDEdit, @RequestParam String txtSubjectNameEdit, @RequestParam String txtIsActiveSubject) {
        Integer parseSubjectID = Integer.parseInt(txtSubjectIDEdit);
        Integer parseCourseID = Integer.parseInt(txtCourseID);
        boolean checked = false;
        if (txtIsActiveSubject.equalsIgnoreCase("true")) {
            checked = true;
        }
        System.out.println(parseCourseID + " update Subject courseID");
        subjectService.updateSubject(parseSubjectID, txtSubjectNameEdit, checked);
        return "redirect:/subjectCreate?courseID=" + parseCourseID;
    }

    @RequestMapping("/pageCourseStudent")
    public ModelAndView loadPageCourseStudent() {
        ModelAndView mav = new ModelAndView("pageCourseStudent");
        List<TblCourse> listPublish = courseService.getCourseActive();
        for (TblCourse tblCourse : listPublish) {
            System.out.println(tblCourse.getCourseName() + " ---(pageCourseTeacher)");
            System.out.println(tblCourse.getCreateUserID() + " ----- List create User ID");
        }
        List<TblUser> listNameTeacher = userService.getTeacher();
        for (int i = 0; i < listNameTeacher.size(); i++) {
            System.out.println(listNameTeacher.get(i) + " teacher full name");
        }
        mav.addObject("listTeacher", listNameTeacher);
        mav.addObject("listCourseActive", listPublish);
        return mav;
    }

    @RequestMapping("/joinCourse")
    public ModelAndView joinCourse() {
        ModelAndView mav = new ModelAndView("lesson");
        return mav;
    }

//    @RequestMapping("/checkPasswordCourse")
//    public ModelAndView checkPasswordJoinCourse(@RequestParam String txtPasswordJoin, @RequestParam String txtCourseID, @RequestParam String txtTeacherID) {
//        ModelAndView mav = new ModelAndView("lesson");
//        String passwordSetting = txtCourseID + txtTeacherID;
//        if (txtPasswordJoin.equalsIgnoreCase(passwordSetting)) {
//            
//            return mav;
//        }
//        ModelAndView mav_1 = new ModelAndView("joinCourse");
//        mav_1.addObject("ERROR_PASSWORD", "Your password is error!!!");
//        return mav_1;
//    }
    @RequestMapping("/viewCourseAdmin")
    public ModelAndView viewCourseAdmin() {
        ModelAndView mav = new ModelAndView("viewCourseAdmin");
        List<TblCourse> list = courseService.getListCourse();
        for (TblCourse tblCourse : list) {
            System.out.println(tblCourse.getCourseName() + " --- Ra roi (pageCourseTeacher)");
        }
        mav.addObject("listCourse", list);
        return mav;
    }

    @RequestMapping("/addClass")
    public ModelAndView addClassPage(@RequestParam String courseID) {
        Integer parseCourseID = Integer.parseInt(courseID);
        TblCourse getCourseItem = courseService.getCourse(parseCourseID);
        ModelAndView mav = new ModelAndView("addClass");
        mav.addObject("COURSE_ITEM", getCourseItem);
        return mav;
    }

    @RequestMapping(value = "/createClass", method = RequestMethod.POST)
    public ModelAndView createClass(@RequestParam String txtCourseID, @RequestParam String txtClassName, @RequestParam String txtNumberOfStudents) throws ParseException {
        Integer parseNumStudent = Integer.parseInt(txtNumberOfStudents);
        java.util.Date date = new java.util.Date();
        Date createDate = new Date(date.getTime());
        Integer parseCourseInt = Integer.parseInt(txtCourseID);
        TblCourse parseCourse = new TblCourse(parseCourseInt);
        TblClass dtoClass = new TblClass(createDate, parseNumStudent, txtClassName, true);
        classService.createNewClass(dtoClass);
        List<Integer> listClassID = classService.getClassIDBottom();
        int maxClassID = listClassID.get(0);
        for (int i = 0; i < listClassID.size(); i++) {
            if (listClassID.get(i).compareTo(maxClassID) > 0) {
                maxClassID = listClassID.get(i);
            }
        }
        System.out.println(maxClassID + " classID lon nhat");
        TblClass parseClassID = new TblClass(maxClassID);
        TblClassCourse dtoClassCourse = new TblClassCourse(parseClassID, parseCourse);
        classCourseService.addClassCourse(dtoClassCourse);
        Integer tblClassCourse = this.classCourseService.getClassCourseID(parseClassID, parseCourse);
        TblClassCourse parseCourseService = new TblClassCourse(tblClassCourse);
        TblStudentCourse tblStudentCourse = new TblStudentCourse("SE142313", 0, createDate, parseCourseService);
        studentCourseService.addStudentJoinCourse(tblStudentCourse);
        return viewCourseAdmin();
    }

    @RequestMapping("/listClass")
    public ModelAndView getAllCourseIDAndClassName() {
        ModelAndView mav = new ModelAndView("listClass");
        List<TblCourse> listCourse = courseService.getListCourse();
        Map<Integer, List<String>> mapClassNameAndCourse = getClassByCourseID();
        mav.addObject("mapClassNameAndCourse", mapClassNameAndCourse);
        mav.addObject("listCourseName", listCourse);
        return mav;
    }

    public Map<Integer, List<String>> getClassByCourseID() {
        Map<Integer, List<String>> listClassAndCourse = new HashMap<>();
        List<Integer> courseID = courseService.getCourseID();
        for (Integer integerCoureID : courseID) {
            TblCourse parseCourseIDObject = new TblCourse(integerCoureID);
            List<String> listClassName = courseService.getListClassNameByCourseID(parseCourseIDObject);
            listClassAndCourse.put(integerCoureID, listClassName);
        }
        return listClassAndCourse;
    }

//    viewListStudentCourse    
    @RequestMapping("/viewListStudentCourse")
    public ModelAndView getAllStudentInClass(@RequestParam String txtClassName, @RequestParam String txtCourseID) {
        ModelAndView mav = new ModelAndView("listClass");
        List<TblCourse> listCourse = courseService.getListCourse();
        Integer parseCourseID = Integer.parseInt(txtCourseID);
        List<TblUser> listStudentJoinCourse = userService.getListStudentJoinCourseID(txtClassName, parseCourseID);
        Map<Integer, List<String>> mapClassNameAndCourse = getClassByCourseID();
        mav.addObject("mapClassNameAndCourse", mapClassNameAndCourse);
        mav.addObject("listCourseName", listCourse);
        mav.addObject("listStudent", listStudentJoinCourse);
        return mav;
    }

//    searchStudentNotJoinClass
//    @RequestMapping("/searchStudentNotJoinClass")
//    public ModelAndView searchStudentNotJoinClass(@RequestParam String txtSearchStudent) {
//        ModelAndView mav = new ModelAndView("listClass");
//        List<TblUser> listStudent = userService.getListSearchStudent(txtSearchStudent);
//        
//        mav.addObject("LIST_STUDENT_CHECK", listStudent);
//        return mav;
//    }
    @RequestMapping("/")
    public ModelAndView loginPage() {
        ModelAndView mav = new ModelAndView("login");        
        return mav;
    }
    @RequestMapping("/checkLogin")
    public ModelAndView checkLogin(HttpSession session, @RequestParam String txtUserID, @RequestParam String txtPassword) {
        ModelAndView mav = new ModelAndView("login");
        TblUser checkLoginDTO = this.userService.checkUserLogin(txtUserID, txtPassword); 
        if(checkLoginDTO != null && checkLoginDTO.getRoleID() == 1){
//            session.setAttribute("USERIDADMIN", checkLoginDTO.getUserID());
            return viewCourseAdmin();
        }else if(checkLoginDTO != null && checkLoginDTO.getRoleID() == 2){ 
//            session.setAttribute("USERIDTEACHER", checkLoginDTO.getUserID());
            return getLoadDashBoard();
        }else if(checkLoginDTO != null && checkLoginDTO.getRoleID() == 3){       
//            session.setAttribute("USERIDSTUDENT", checkLoginDTO.getUserID());
            return loadPageCourseStudent();
        }
        mav.addObject("ERROR_LOGIN", "Your userID or password is incorrect!!!");
        return mav;
    }
    
}
