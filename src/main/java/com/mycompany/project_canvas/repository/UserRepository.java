/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblUser;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Asus
 */
public interface UserRepository extends JpaRepository<TblUser, String> {

    @Query(value = "SELECT t FROM TblUser t WHERE t.roleID = 2")
    public List<TblUser> getListTeacher();

//    @Query(value = "SELECT t FROM TblUser t WHERE t.fullName LIKE '%' || :keyword || '%' "
//            + "OR t.email LIKE '%' || :keyword || '%' "
//            + "OR t.userID LIKE '%' || :keyword || '%' "
//            + "OR t.phoneNum LIKE '%' || :keyword || '%' "
//            + "AND t.roleID = '3'")
//    public List<TblUser> searchStudent(@Param("keyword") String keyword);
    @Query(value = "SELECT j FROM TblStudentCourse i, TblUser j WHERE i.classCourseID = "
            + "(SELECT b.classCourseID FROM TblCourse a, TblClassCourse b,  TblClass c WHERE c.className = :className "
            + "and a.courseID = :courseID and a.courseID = b.courseID and b.classID = c.classID ) and i.studentJoinID = j.userID")
    public List<TblUser> listStudentJoinID(@Param("className") String className, @Param("courseID") Integer courseID);

    @Query(value = "SELECT t FROM TblUser t WHERE t.userID = :userID")
    public List<TblUser> getListStudentJoinCourse(@Param("userID") String userID);
    
    @Query(value = "SELECT t FROM TblUser t WHERE t.userID = :userID AND t.password = :password")
    public TblUser checkLogin(@Param("userID") String userID, @Param("password") String password);
    
}
