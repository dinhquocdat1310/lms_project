/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblClass;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Asus
 */
public interface ClassRepository extends JpaRepository<TblClass, Integer>{

    @Query(value = "SELECT t.classID FROM TblClass t ORDER BY t.classID DESC")
    public List<Integer> getListClassIDBottom();

    
}
