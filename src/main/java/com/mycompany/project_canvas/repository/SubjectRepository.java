/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblCourse;
import com.mycompany.project_canvas.dbo.TblSubject;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Asus
 */
public interface SubjectRepository extends JpaRepository<TblSubject, Integer>{
    @Modifying
    @Query(value = "UPDATE TblSubject t SET t.subjectName = :subjectName, t.isActive = :isActive WHERE t.subjectID = :subjectID")
    @Transactional
    public void updateSubjectByID(@Param("subjectName") String subjectName, @Param("isActive") boolean isActive, @Param("subjectID") Integer subjectID);

    @Query(value = "SELECT t FROM TblSubject t WHERE t.courseID = :courseID")
    public List<TblSubject> getListSubjectByCourseID(@Param("courseID") TblCourse courseID);

    @Override
    public void deleteById(Integer id);

    @Override
    public List<TblSubject> findAll();

    @Override
    public Optional<TblSubject> findById(Integer id);
}
