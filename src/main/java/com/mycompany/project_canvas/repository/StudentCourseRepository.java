/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblStudentCourse;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author Asus
 */
public interface StudentCourseRepository extends JpaRepository<TblStudentCourse, String>{
    
//    @Query(value = "SELECT t.classCourse FROM TblClassCourse t "
//            + "WHERE t.classID IN(SELECT c.classID FROM TblClassCourse c WHERE c.courseID = :courseID)")
//    public List<String> getListClassName(@Param("courseID") TblCourse courseID);
}
