/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblLession;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Asus
 */
public interface LessonRepository extends JpaRepository<TblLession, Integer>{
    
    
}
