/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblClass;
import com.mycompany.project_canvas.dbo.TblClassCourse;
import com.mycompany.project_canvas.dbo.TblCourse;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Asus
 */
public interface ClassCourseRepository extends JpaRepository<TblClassCourse, Integer>{
    
    @Query(value = "SELECT t.classCourseID FROM TblClassCourse t WHERE t.classID = :classID AND t.courseID = :courseID")
    public Integer getListSubjectByCourseID(@Param("classID") TblClass classID, @Param("courseID") TblCourse courseID);

    @Query(value = "SELECT t.classID FROM TblClassCourse t WHERE t.courseID = :courseID")
    public List<Integer> getListClassIDByCourseID(@Param("courseID") TblCourse courseID);
}
