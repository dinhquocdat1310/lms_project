/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.project_canvas.repository;

import com.mycompany.project_canvas.dbo.TblCourse;
import java.sql.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Asus
 */
public interface CourseRepository extends JpaRepository<TblCourse, Integer> {
    
    @Modifying
    @Query(value = "UPDATE TblCourse t SET t.courseName = :courseName, t.description = :description, t.startAt = :startAt, t.isActive = :isActive WHERE t.courseID = :courseID")   
    @Transactional
    public void updateCourseByID(@Param("courseName") String courseName, @Param("description") String description, @Param("startAt") Date startAt, @Param("isActive") boolean isActive, @Param("courseID") Integer courseID);
    
    @Query(value = "SELECT t FROM TblCourse t WHERE t.isActive = 'true'")
    public List<TblCourse> getListCoursePublish();
    
    @Query(value = "SELECT t FROM TblCourse t WHERE t.isActive = 'false'")
    public List<TblCourse> getListCourseUnpublish();       
        
    @Override
    public List<TblCourse> findAll();                
    
    @Query(value = "SELECT DISTINCT t.className FROM TblClass t "
            + "WHERE t.classID IN(SELECT c.classID FROM TblClassCourse c WHERE c.courseID = :courseID)")
    public List<String> getListClassName(@Param("courseID") TblCourse courseID);
    
    @Query(value = "SELECT t.courseID FROM TblCourse t")
    public List<Integer> getListCourseID(); 
}
